-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP PROCEDURE IF EXISTS `abandon_hourly_report`;;
CREATE PROCEDURE `abandon_hourly_report`(IN _user_id INT(11), IN `_queue_id` INT(30), IN `_date` VARCHAR (50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE i INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
DECLARE _columnname VARCHAR (50) ;
DECLARE query VARCHAR (500) ;
SET hours=0;
SET i=1;
WHILE i<25 DO
SET _columnname = (SELECT Concat(i, '-hour'));
IF _queue_id != 'All' THEN

IF i = 1 THEN

SET @query = CONCAT('INSERT INTO tbl_abn_hourly_report ( `user_id`,`date`,`queue_id`, `', _columnname,'`
)
SELECT
',QUOTE(_user_id),',
',QUOTE(_date),',
',QUOTE(_queue_id),',
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = ',QUOTE(i),'
AND tbl_calls_evnt.agnt_queueid = ',QUOTE(_queue_id),'
AND `tbl_calls_evnt`.`date` = ',QUOTE(_date),'  limit 1;');

PREPARE stmt FROM @query;
EXECUTE stmt;
ELSE

SET hours = (SELECT
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = i
AND tbl_calls_evnt.agnt_queueid = _queue_id
AND `tbl_calls_evnt`.`date` = _date  limit 1);


SET @query = CONCAT('UPDATE tbl_abn_hourly_report SET  `', _columnname,'` = "', hours,'"
WHERE `tbl_abn_hourly_report`.`date` = ',QUOTE(_date));

PREPARE stmt FROM @query;
EXECUTE stmt;

END IF;
SET i = i + 1;
ELSE
IF i = 1 THEN

SET @query = CONCAT('INSERT INTO tbl_abn_hourly_report ( `user_id`,`date`,`queue_id`, `', _columnname,'`
)
SELECT
',QUOTE(_user_id),',
',QUOTE(_date),',
',QUOTE(_queue_id),',
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = ',QUOTE(i),'
AND `tbl_calls_evnt`.`date` = ',QUOTE(_date),'  limit 1;');

PREPARE stmt FROM @query;
EXECUTE stmt;
ELSE

SET hours = (SELECT
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = i
AND `tbl_calls_evnt`.`date` = _date  limit 1);


SET @query = CONCAT('UPDATE tbl_abn_hourly_report SET  `', _columnname,'` = "', hours,'"
WHERE `tbl_abn_hourly_report`.`date` = ',QUOTE(_date));

PREPARE stmt FROM @query;
EXECUTE stmt;

END IF;
SET i = i + 1;
END IF;

END WHILE;

End;;

DROP PROCEDURE IF EXISTS `agent_detail_repo_agentwise`;;
CREATE PROCEDURE `agent_detail_repo_agentwise`(IN `_user_id` int(11), IN `_com_id` int(11), IN `_agent_id` int(11), IN `_to_date` varchar(50), IN `_frm_date` varchar(50))
BEGIN
											INSERT INTO tbl_agent_detail_report (
														`userid`,
														`str_evntid`,
														`end_event`,
														`login_duration_dec`,
														`login_duration`,
														`break_time_sec`,
														`online_time_sec`,
														`break_time`,
														`answer_sec`,
														`answer_tot_calls`,
														`answer_ind_calls`,
														`answer_out_calls`,
														`acw_sec`,
														`acw_times`,
														`logout_datetime`,
														`login_datetime`,
														`user`,
														`username`,
														`active_time`,
														`idle_time`,
														`notready_time`
														)
											SELECT
											`tbl_agnt_evnt`.`agnt_userid` AS `userid`,
											`tbl_agnt_evnt`.`id` AS `str_evntids`,
											( SELECT tbl_agnt_evnt.agnt_event AS end_event FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS end_event,
											( SELECT ( tbl_agnt_evnt.evnt_min_count * 60 ) AS evnt_min_count FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS login_duration_dec,
											(
												SELECT
													SEC_TO_TIME(
													ROUND( login_duration_dec ))) AS login_duration,
											(
												SELECT
													( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
												FROM
													tbl_agnt_evnt 
												WHERE
													( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
													AND tbl_agnt_evnt.agnt_event = "Break End" 
													AND tbl_agnt_evnt.agnt_userid = userid 
												) AS break_time_sec,
											  (
													SELECT
														( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
													FROM
														tbl_agnt_evnt 
													WHERE
														( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
														AND tbl_agnt_evnt.agnt_event = "Offline" 
														AND tbl_agnt_evnt.agnt_userid = userid 
													) AS online_time_sec,
													(
													SELECT
														SEC_TO_TIME(
														ROUND( break_time_sec ))) AS break_time,
		                       (
														SELECT
															(
															SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec 
														FROM
															tbl_calls_evnt 
														WHERE
															tbl_calls_evnt.agnt_userid = userid 
															AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
															AND tbl_calls_evnt.STATUS = 'ANSWER' 
														) AS answer_sec,
														(
																	SELECT
																		(
																		count( tbl_calls_evnt.id )) AS answer_tot_calls 
																	FROM
																		tbl_calls_evnt 
																	WHERE
																		tbl_calls_evnt.agnt_userid = userid 
																		AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																		AND tbl_calls_evnt.STATUS = 'ANSWER' 
																	) AS answer_tot_calls,
																	(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_ind_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'Inbound' 
																		) AS answer_ind_calls,
																		(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_out_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'outbound' 
																		) AS answer_out_calls,
																	  (
																			SELECT
																				SEC_TO_TIME(
																				ROUND( answer_sec ))) AS answer_times,
																			(
																			SELECT
																				(
																				SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec 
																			FROM
																				tbl_calls_evnt 
																			WHERE
																				tbl_calls_evnt.agnt_userid = userid 
																				AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																				AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			) AS acw_sec,
																			( SELECT tbl_agnt_evnt.cre_datetime AS logout_datetime FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS logout_datetime,
	                    `tbl_agnt_evnt`.`cre_datetime` AS `login`,
											_user_id,
	                    `user_master`.`username`, 
											(
	SELECT
		SEC_TO_TIME(
			ROUND(
			IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 )))) AS active_time,
		(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 ))))) AS idle_time,
			(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( online_time_sec, 0 ))))) AS notready_time	
FROM
	`tbl_agnt_evnt`
	INNER JOIN `user_master` ON `user_master`.`id` = `tbl_agnt_evnt`.`agnt_userid` 
WHERE
	`tbl_agnt_evnt`.`cre_datetime` BETWEEN _frm_date 
	AND _to_date 
	AND `tbl_agnt_evnt`.`agnt_event` = 'Log IN Time' 
	AND `user_master`.`com_id` = _com_id
AND `tbl_agnt_evnt`.`agnt_userid` = _agent_id	
	AND `tbl_agnt_evnt`.`id_of_prtone` IS NULL;
  
End;;

DROP PROCEDURE IF EXISTS `createBatch`;;
CREATE PROCEDURE `createBatch`()
BEGIN

END;;

DROP PROCEDURE IF EXISTS `prcAbandon`;;
CREATE PROCEDURE `prcAbandon`(
  IN _callid VARCHAR(40))
BEGIN
  CALL prcDeleteQueue(_callid);
END;;

DROP PROCEDURE IF EXISTS `prcCallComplete`;;
CREATE PROCEDURE `prcCallComplete`(
  IN _agent int(6),
  IN _callid varchar(40),
  IN _dst_number int(9),
  IN _queue_id int(6))
BEGIN
  CALL prcDeleteQueue(_callId);
  CALL prcLiveSend(_agent,NULL,NULL);
END;;

DROP PROCEDURE IF EXISTS `prcCallConnect`;;
CREATE PROCEDURE `prcCallConnect`( 
  IN _callid VARCHAR(40),
  IN _agent VARCHAR(20), 
  IN _data VARCHAR(100))
    MODIFIES SQL DATA
BEGIN

  UPDATE queue_log SET
    time = NOW(),
    agent = _agent,
    event = 'CONNECT'
    WHERE callid = _callid;

  UPDATE live_status SET status='CONNECTED' WHERE sip_id = _agent;

END;;

DROP PROCEDURE IF EXISTS `prcCreateCamp`;;
CREATE PROCEDURE `prcCreateCamp`(IN _batch_id INT(11))
BEGIN

DELETE FROM `phonikip_db`.`autodial_batch` where `call_schedule`='0000-00-00 00:00:00';

INSERT IGNORE INTO autodial_master (campaign_id, cus_number, cus_name, cus_nic, cus_type, cus_add, cus_cover_date, cus_deduction)
SELECT batch_id, cus_number, cus_name, cus_nic, cus_type, cus_address, cus_cover_date, cus_deduction FROM autodial_csv_data WHERE batch_id=_batch_id;

SET @n=(SELECT MAX(id) FROM autodial_csv_data WHERE batch_id=_batch_id);
SET @i=(SELECT MIN(id) FROM autodial_csv_data WHERE batch_id=_batch_id);
SET @batch_id=_batch_id;
 
PREPARE stmt FROM
"SELECT cus_number
FROM autodial_csv_data
WHERE batch_id=@batch_id AND id=?
INTO @cusNumber";
 
WHILE @i<=@n DO
EXECUTE stmt USING @i;
 
INSERT IGNORE INTO autodial_batch (campaign_id, cus_number)
SELECT campaign_id, cus_number
FROM (
SELECT batch_id AS campaign_id,
cus_number AS cus_number
FROM autodial_csv_data
WHERE batch_id=_batch_id AND id=@i
)t
WHERE NOT EXISTS(SELECT 1 FROM autodial_remark t2
WHERE (t2.callstatus = 'ANSWERED' OR t2.attempt>3) and t2.cus_number=@cusNumber);

SET @i=@i+1;

END WHILE;
DELETE FROM `phonikip_db`.`autodial_csv_data` WHERE `batch_id`=_batch_id;
END;;

DROP PROCEDURE IF EXISTS `prcDeleteQueue`;;
CREATE PROCEDURE `prcDeleteQueue`(_callid VARCHAR(40))
    MODIFIES SQL DATA
BEGIN
    DELETE FROM queue_log WHERE callid=_callid;
END;;

DROP PROCEDURE IF EXISTS `prcEnterQueue`;;
CREATE PROCEDURE `prcEnterQueue`(
  IN _created DATETIME, 
  IN _callid VARCHAR(40),
  IN _queuename VARCHAR(20),
  IN _agent VARCHAR(20),
  IN _event VARCHAR(20),
  IN _data VARCHAR(100))
    MODIFIES SQL DATA
BEGIN
  INSERT INTO queue_log (time,callid,queuename,agent,event,data) 
  values (_created,_callid,_queuename,_agent,_event,_data);
END;;

DROP PROCEDURE IF EXISTS `prcLiveSend`;;
CREATE PROCEDURE `prcLiveSend`(
  IN _sip_id int(6),
  IN _cli  INT(14),
  IN _queuename varchar(45))
BEGIN

  UPDATE live_status
  SET cli = _cli,
      queuename = _queuename
  WHERE sip_id = _sip_id;

END;;

DROP PROCEDURE IF EXISTS `prcRingNoAnswer`;;
CREATE PROCEDURE `prcRingNoAnswer`(IN _callid VARCHAR(40))
BEGIN

END;;

DROP PROCEDURE IF EXISTS `queue_abandon_hourly_report`;;
CREATE PROCEDURE `queue_abandon_hourly_report`(IN _user_id INT(11), IN `_queue_id` INT(30), IN _frm_date VARCHAR(50), IN _to_date VARCHAR (50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE i INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
DECLARE _columnname VARCHAR (50) ;
DECLARE query VARCHAR (500) ;
SET hours=0;
SET i=1;
WHILE i<25 DO
SET _columnname = (SELECT Concat(i, '-hour'));

IF i = 1 THEN

SET @query = CONCAT('INSERT INTO tbl_abn_hourly_report ( `user_id`,`queue_id`, `', _columnname,'`)
SELECT
',QUOTE(_user_id),',
',QUOTE(_queue_id),',
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = ',QUOTE(i),'
AND tbl_calls_evnt.agnt_queueid = ',QUOTE(_queue_id),'
AND `tbl_calls_evnt`.`cre_datetime` BETWEEN ',QUOTE(_frm_date),' AND ',QUOTE(_to_date),' limit 1;');

PREPARE stmt FROM @query;
EXECUTE stmt;
ELSE

SET hours = (SELECT
COUNT(`tbl_calls_evnt`.`linkedid`) AS `rec_linkedid`

FROM
`tbl_calls_evnt`
WHERE
`tbl_calls_evnt`.`desc` = "ABANDON"
AND HOUR ( tbl_calls_evnt.cre_datetime ) = i
AND tbl_calls_evnt.agnt_queueid = _queue_id
AND `tbl_calls_evnt`.`cre_datetime` BETWEEN  _frm_date AND _to_date limit 1);


SET @query = CONCAT('UPDATE tbl_abn_hourly_report SET  `', _columnname,'` = "', hours,'"
WHERE `tbl_abn_hourly_report`.`queue_id` = ',QUOTE(_queue_id));

PREPARE stmt FROM @query;
EXECUTE stmt;

END IF;
SET i = i + 1;


END WHILE;

End;;

DROP PROCEDURE IF EXISTS `queue_hourly`;;
CREATE PROCEDURE `queue_hourly`(IN `_user_id` int(11), IN `_com_id` int(11), IN `_to_date` varchar(50), IN `_frm_date` varchar(50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE incre_rowcount INT Default 0 ;
DECLARE rowcount INT Default 0 ;
DECLARE extension VARCHAR (50) ;
DECLARE i INT DEFAULT 0;
SET hours=0;
SET i=0;


		SET  rowcount := (SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` );

		exten_loop: LOOP
								
		IF incre_rowcount < 3 THEN
		
		SET  extension := (SELECT `queues_config`.`extension` FROM `asterisk`.`queues_config` LIMIT incre_rowcount,1 );
		
							simple_loop: LOOP
								
							 IF hours < 24 THEN
							 
							 
									INSERT INTO tbl_queue_hrly_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														
														`answer_acwtimes`,
														
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														
														
														
														
														`hold_calls`,
														`hold_time`,
														
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														
														)
										SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS offerd_calls,
										      
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND (
												    HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											      AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls_sl,
													
													
													
													
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)
												GROUP BY
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
											
											

								
								
								
							 SET hours=hours+1;
							 ELSE 
							 LEAVE simple_loop;
							 END IF;
				 END LOOP simple_loop;
				
				
		SET incre_rowcount=incre_rowcount+1;
		ELSE 
			LEAVE exten_loop;
		END IF;
		END LOOP exten_loop;


END;;

DROP PROCEDURE IF EXISTS `queue_repo_day`;;
CREATE PROCEDURE `queue_repo_day`(IN `_user_id` int(11), IN `_com_id` int(11), IN `_to_date` char(50), IN `_frm_date` varchar(50))
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
SET i=0;
WHILE i<n DO 
											INSERT INTO tbl_queue_day_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														
														`answer_acwtimes`,
														
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														
														
														
														
														`hold_calls`,
														`hold_time`,
														
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														
														)
											SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS offerd_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_abn_calls_sl,
													
													
													
													
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
												GROUP BY
													`tbl_calls_evnt`.`date`,
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
  SET i = i + 1;
END WHILE;
End;;

DROP PROCEDURE IF EXISTS `queue_repo_hourly`;;
CREATE PROCEDURE `queue_repo_hourly`(IN `_user_id` int(11), IN `_com_id` int(11), IN `_to_date` varchar(50), IN `_frm_date` varchar(50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE i INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
SET hours=0;
SET i=0;

SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
WHILE i<n DO 
simple_loop: LOOP
								
							 IF hours < 24 THEN
							 
							 
									INSERT INTO tbl_queue_hrly_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														
														`answer_acwtimes`,
														
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														
														
														
														
														`hold_calls`,
														`hold_time`,
														
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														
														)
										SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS offerd_calls,
										      
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND (
												    HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											      AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls_sl,
													
													
													
													
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS transout_calls,
													hours,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
												`queues_config`.`com_id` = _com_id 
													AND
										(
											`tbl_calls_evnt`.`date` between _frm_date and _to_date AND 
											HOUR ( tbl_calls_evnt.cre_datetime ) = hours AND tbl_calls_evnt.agnt_queueid = extension 
										) GROUP BY tbl_calls_evnt.agnt_queueid;
										IF ROW_COUNT() = 0 THEN
							
										INSERT INTO tbl_queue_hrly_report (
															`userid`,
															`extension`,
															`descr`,
															`date_or_hour`
														)
														SELECT
															_user_id,
														`queues_config`.`extension`,
														`queues_config`.`descr`,
														hours
											
														FROM
															`asterisk`.`queues_config` where  `queues_config`.`extension` = extension
															AND `asterisk`.`queues_config`.`com_id` = _com_id;
								END IF; 		
										

								
								
								
							 SET hours=hours+1;
							 ELSE 
							 LEAVE simple_loop;
							 END IF;
				 END LOOP simple_loop;
				
				
		 SET i = i + 1;
END WHILE;
End;;

DROP PROCEDURE IF EXISTS `queue_repo_sum`;;
CREATE PROCEDURE `queue_repo_sum`(IN `_user_id` int(11), IN `_com_id` int(11), IN `_to_date` varchar(50), IN `_frm_date` varchar(50))
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
SET i=0;
WHILE i<n DO 
											INSERT INTO tbl_queue_summary_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														
														`answer_acwtimes`,
														
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														
														
														
														
														`hold_calls`,
														`hold_time`,
														
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														
														)
											SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS offerd_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS tot_abn_calls_sl,
													
													
													
													
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
												GROUP BY
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
  SET i = i + 1;
END WHILE;
End;;

DROP PROCEDURE IF EXISTS `ROWORDER`;;
CREATE PROCEDURE `ROWORDER`(IN con int(10))
BEGIN

DELETE FROM rcl_order_master
WHERE o_id Not IN (SELECT ordernum FROM rcl_autodial_csv_data where batch_id=con) and camp_id=(select campaign_id from rcl_autodial_csv_master where batch_id=con);

INSERT INTO rcl_order_master(o_id, o_shrm, o_date, o_cus_num, o_cus_name, o_cus_phone,batch_id,shr_phone,camp_id)
SELECT t.ordernum, t.showrm, t.orderdate, t.cusnum, t.cusname, t.cusphonenum,t.batch_id,t.shr_phone,camp_id
FROM rcl_autodial_csv_data t WHERE t.batch_id=con 
ON DUPLICATE KEY UPDATE o_shrm=t.showrm,o_date=t.orderdate,o_cus_num=t.cusnum,o_cus_name=t.cusname,o_cus_phone=t.cusphonenum,batch_id=t.batch_id,camp_id=t.camp_id,shr_phone=t.shr_phone;
 
INSERT INTO rcl_order_items(o_id,l_no,i_code,i_desc,i_qty,i_schedship_date,i_resdate,i_foresdate,i_whcode,i_whname,i_cpcode,i_cpname,l_status,batch_id,i_res_qty,i_age_days,so,iso,ins_qty,shr_phone) SELECT ordernum,linenum,item_code,item_description,qty,ship_date,reservaion_date,fullorder_reservaion_date,warehouse_code,warehouse_name,cpc,cpn,linestatus,batch_id,res_qty,age_days,so,iso,ins_qty,shr_phone FROM rcl_autodial_csv_data where batch_id=con order by ordernum desc;
End;;

DELIMITER ;

CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(45) NOT NULL,
  `form_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `section` (`id`, `section`, `form_name`) VALUES
(1,	'Manage User',	'users'),
(2,	'User Type',	'usertype'),
(3,	'Uer Privilage',	'privilagetype'),
(4,	'Live Report',	'livereport'),
(5,	'QueueReport',	'queuereport'),
(6,	'Queue Profile Report',	'queueprofile'),
(7,	'Queue Detail Report',	'queuedetail'),
(8,	'Abondon Call Report',	'abondoncall'),
(9,	'Break Report',	'breakreport'),
(10,	'Login Logout Report',	'loginlogout'),
(11,	'Activity Report',	'activity'),
(12,	'Call Traffic Report',	'calltraffic'),
(13,	'Call Hangup Report',	'callhangup'),
(14,	'Detail Call Log',	'detailcalllog'),
(15,	'Setting',	'setting'),
(16,	'Upload CSV',	'uploadcsv'),
(17,	'Campaign',	'campaign'),
(18,	'Data Download',	'datadownload'),
(19,	'Schema',	'schema'),
(20,	'Inbound Call Register',	'inboundcalls'),
(21,	'View Tickets',	'ViewTickets'),
(22,	'Ticket Details',	'ViewTicket'),
(23,	'Update Ticket Status',	'UpdateTicketStatus'),
(24,	'View Contacts',	'ViewContacts'),
(25,	'Add Contact',	'AddContact'),
(26,	'Edit Contact',	'EditContact'),
(27,	'Add Call Log',	'addcalllog'),
(28,	'View Call Log',	'ViewCallLog'),
(29,	'Add Ticket',	'AddTicket'),
(30,	'View Departments',	'ViewDepartments'),
(31,	'Add Department',	'AddDepartment'),
(32,	'Edit Department',	'EditDepartment'),
(33,	'View Subdepartment',	'ViewSubdepartments'),
(34,	'Edit Subdepartment',	'EditSubdepartment'),
(35,	'Add Subdepartment',	'AddSubdepartment'),
(36,	'Transfer Ticket',	'TransferTicket'),
(37,	'User Allocation',	'UserAllocation'),
(38,	'Callback Report',	'CallbackReport'),
(39,	'View Ticket Categories',	'ViewTicketCategories'),
(40,	'Add Ticket Category',	'AddTicketCategory'),
(41,	'Contact Upload',	'ContactUpload'),
(42,	'View Contact Types',	'ViewContactTypes'),
(43,	'Add Contact Type',	'AddContactType'),
(44,	'Add Phonebook Contact',	'AddPhonebookContact'),
(45,	'View Phonebook Contact',	'ViewPhonebookContacts'),
(46,	'Edit Phonebook Contact',	'EditPhonebookContact'),
(47,	'Reset Password',	'ResetPassword'),
(48,	'Call Recording',	'CallRecording'),
(49,	'Campaign Manage',	'viewcampaign'),
(50,	'Order Master Report',	'ordermaster'),
(51,	'Campaign Summary',	'campaignsummary'),
(52,	'Agent Campaign Summary',	'agentcampsummary'),
(53,	'Activity Detail',	'activitydetail'),
(54,	'scheddash',	'scheddash'),
(55,	'inboundoutbound',	'inboundoutbound'),
(56,	'campaign',	'campaign'),
(57,	'campaigns',	'campaigns'),
(58,	'Supervisor Dashboard Unit01',	'sdashboardunit1'),
(59,	'Supervisor Dashboard Unit02',	'sdashboardunit2'),
(60,	'Supervisor Dashboard Master',	'sdashboardmaster'),
(61,	'Disposition Report',	'disposition'),
(62,	'lvlonedashboard',	'Sale Channel DB'),
(63,	'lvltwodashboard',	'Location DB'),
(64,	'lvlthrdashboard',	'Product Category DB'),
(65,	'contacts',	'Contact Register'),
(66,	'searchCustomer',	'Ticket Entry'),
(67,	'ticketTransferDashboard',	'Reallocations'),
(68,	'toBeCompleteDashBoard',	'Followed-Up'),
(69,	'viewlevelone',	'Sale Channel Entry'),
(70,	'viewleveltwo',	'Location Entry'),
(71,	'viewlevelthr',	'Product Category Entry'),
(72,	'viewkpi_info',	'KPI Level Info Entry'),
(73,	'levelWiseUserAllocation',	'Supervisor Allocation Entry'),
(74,	'levelWiseaAgentAllocation',	'Agent Allocation Entry'),
(75,	'addnewcontact',	'Add New Contact'),
(76,	'addcalllog',	'Add Calllog View'),
(77,	'tickets',	'View Tickets'),
(78,	'ticket',	'View Ticket Details'),
(79,	'popupcallhistory',	'View Call History'),
(80,	'addcontact',	'Edit Contact View'),
(81,	'viewaddticket',	'View Add Ticket'),
(82,	'add_levelone_view',	'Level one Entry View'),
(83,	'add_leveltwo_view',	'Level two Entry View'),
(84,	'users',	'View User List'),
(85,	'usertype',	'View User Types'),
(86,	'privilagetype',	'View Privilage Type'),
(87,	'presetremarks',	'View Preset Remarks'),
(88,	'resetpassword',	'View Reset Password'),
(89,	'callrecords',	'Call Recording View'),
(90,	'slareports',	'View Sale Report'),
(91,	'uploadcsv',	'View Upload CSV'),
(92,	'campaigns',	'View Campaigns'),
(93,	'datadownload',	'Campaign Data Download View'),
(94,	'smscampaigns',	'Sms Campaign View'),
(95,	'add_levelthr_view',	'Add Product Category View'),
(96,	'add_kpi_info_view',	'Add KPI Level Info View'),
(97,	'inboundCallUpdateRecordInfo',	'inboundCallUpdateRecordInfo'),
(98,	'inboundcalls',	'inboundcalls'),
(99,	'lvlthragentdashboard',	'lvlthragentdashboard'),
(100,	'ticketTransferDashboard',	'ticketTransferDashboard'),
(101,	'viewtransferticket',	'viewtransferticket'),
(102,	'Cspuser_allocation',	'Cspuser_allocation'),
(103,	'TicketdetailsDashboard',	'TicketdetailsDashboard'),
(104,	'getexcelticket',	'getexcelticket'),
(105,	'toBeCompleteDashBoard',	'toBeCompleteDashBoard'),
(106,	'view_findcallhistory',	'view_findcallhistory'),
(107,	'View Complete Ticket',	'viewCompleteticket'),
(108,	'emailsrvdashboard',	'emailsrvdashboard'),
(109,	'archemailsrvdashboard',	'archemailsrvdashboard'),
(110,	'srvemailAgentAllocation',	'srvemailAgentAllocation'),
(111,	'smssrvdashboard',	'smssrvdashboard'),
(112,	'archsmssrvdashboard',	'archsmssrvdashboard'),
(113,	'view_srvyqn',	'view_srvyqn'),
(114,	'remainderdashboard',	'remainderdashboard'),
(115,	'faxsrvdashboard',	'faxsrvdashboard'),
(116,	'archfaxsrvdashboard',	'archfaxsrvdashboard'),
(117,	'QnreportDashboard',	'QnreportDashboard'),
(118,	'CallcampagntDashboard',	'CallcampagntDashboard'),
(119,	'CallcampagntsummryDashboard',	'CallcampagntsummryDashboard'),
(120,	'view_sipreglist',	'view_sipreglist'),
(121,	'view_queue_report',	'view_queue_report'),
(122,	'view_queue_det_report',	'view_queue_det_report'),
(123,	'view_agntactionhistory',	'view_agntactionhistory'),
(124,	'view_abncall_report',	'view_abncall_report'),
(125,	'view_outboundcall_report',	'view_outboundcall_report'),
(126,	'view_calls_det_report',	'view_calls_det_report'),
(127,	'view_agntcallhistory',	'view_agntcallhistory'),
(128,	'Agentdashboard',	'Agentdashboard'),
(129,	'view_agnt_det_report',	'view_agnt_det_report'),
(130,	'incomplete',	'incomplete'),
(131,	'CampaignDataDownload',	'datadownload'),
(132,	'donotCallNos',	'donotCallNos'),
(133,	'donotcall',	'donotcall'),
(134,	'viewEmailTemplates',	'viewEmailTemplates'),
(135,	'addEmailTemplates',	'addEmailTemplates'),
(136,	'viewEmailAttachment',	'viewEmailAttachment'),
(137,	'addnewemailAttachment',	'addnewemailAttachment'),
(138,	'viewqueueallocation',	'viewqueueallocation'),
(139,	'view_srvyqn',	'view_srvyqn'),
(140,	'csp_users',	'csp_users'),
(141,	'Archive WhatsApp SRV Dashboard',	'ArchiveWhatsAppSRV'),
(142,	'WhatsApp SRV Dashboard',	'WhatsappSRV'),
(143,	'Read Whatsapp Chat',	'ReadWhatsappChat'),
(144,	'Callback Request',	'callback_request_report'),
(145,	'Abandon Callback Report',	'view_abncallback_report'),
(146,	'IVR Report',	'ivrreport'),
(147,	'IVR Drop Detail Report',	'IVRDropDetail'),
(148,	'View Level Three',	'viewlevelthr'),
(150,	'View Evaluation Criteria Type',	'view_eval_crit_type_list'),
(151,	'View Evaluation Criteria Master List',	'view_eval_crit_mst_list'),
(152,	'View Abandon Hourly',	'view_abn_hourly_report'),
(153,	'View Queue Abandon Hourly',	'view_queue_abn_hourly_report'),
(154,	'IVR Extension Transfer Report',	'ivrextTransreport'),
(155,	'Occupancy Report',	'occupancy_report'),
(156,	'ArchiveWhatsAppsrvDashboard',	'ArchiveWhatsAppsrvDashboard'),
(157,	'view_agentaction_summ_report',	'view_agentaction_summ_report'),
(158,	'view_br_exceed_repo',	'view_br_exceed_repo'),
(159,	'View Call Summary Report',	'view_call_sum_repo'),
(161,	'view_add_eval_crit_type',	'view_add_eval_crit_type'),
(162,	'view_eval_cntxt',	'view_eval_cntxt'),
(163,	'view_evaluationContextMaster',	'view_evaluationContextMaster'),
(164,	'View Agent Ring Detail Report',	'view_agnt_ring_detail_report'),
(165,	'view_agnt_eval_mst_list',	'view_agnt_eval_mst_list'),
(166,	'view_add_agnt_eval_mst',	'view_add_agnt_eval_mst'),
(167,	'view_exten_allocation',	'view_exten_allocation'),
(168,	'',	'view_extension_list'),
(169,	'View Extension List',	'view_extension_list'),
(170,	'View Phone Book',	'view_phone_bk'),
(171,	'View User Action',	'view_useraction'),
(172,	'Queue Waiting Time Report',	'queue_waiting_time_report'),
(173,	'Call Rating Details Report',	'view_call_rate_det'),
(174,	'View Call Rating Summary Agent ',	'view_call_rate_summ_agnt'),
(175,	'View Call Rating Summary Queue',	'view_call_rate_summ_queue'),
(176,	'Evaluation criteria Master',	'view_eval_crit_mst_list'),
(177,	'View Add Evaluation Criteria',	'view_add_eval_crit_mst'),
(178,	'view_voicemail_detail_report',	'view_voicemail_detail_report'),
(179,	'view_ticket_summary',	'view_ticket_summary'),
(180,	'Agent Ticket Reports',	'lvlthragentdashboard_repo'),
(181,	'View Break Approval List',	'view_br_approval_master'),
(182,	'View Add Break Approval',	'view_add_br_approval'),
(183,	'View Break Request - sup Action',	'view_br_req_approval'),
(184,	'view_queue_allocation',	'view_queue_allocation'),
(185,	'View Edit Break Approval',	'view_edit_br_appr_shedule');

-- 2021-08-17 12:46:57
