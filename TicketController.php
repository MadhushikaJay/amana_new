<?php
/**
 * Created by PhpStorm.
 * User: Prashan
 * Date: 24-Jul-17
 * Time: 9:21 AM
 */

namespace App\Http\Controllers;
use Carbon\Carbon;
use DB;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use App\Http\Util;

class TicketController
{
    public function viewTickets(){
        if(Util::isAuthorized("tickets")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("tickets")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Tickets","View");

	    $lvloneid_list = "";
        $lvltwoid_list = "";
        $lvlthrid_list = "";
        $getlvlone=array();
        $getlvltwo=array();
        $getlvlthr=array();
        $getlvloneids =DB::select("SELECT   tbl_user_allocation.level_one,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));
        
        if(!empty($getlvloneids[0]->level_one))
        {
            $lvloneid_list = $getlvloneids[0]->level_one;
            $getlvlone =DB::select("SELECT   tbl_levelone.id,
                                            tbl_levelone.name,
                                            tbl_levelone.status
                                            FROM
                                            tbl_levelone
                                            WHERE
                                            tbl_levelone.id iN ($lvloneid_list)");

        }

        $getlvltwoids =DB::select("SELECT   tbl_user_allocation.level_two,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));

        if(!empty($getlvltwoids[0]->level_two))
        {
            $lvltwoid_list = $getlvltwoids[0]->level_two;
            $getlvltwo =DB::select("SELECT   tbl_leveltwo.id,
                                                tbl_leveltwo.name,
                                                tbl_leveltwo.status
                                                FROM
                                                tbl_leveltwo
                                                WHERE
                                                tbl_leveltwo.id iN ($lvltwoid_list)");  

        }

        $getlvlthrids =DB::select("SELECT   tbl_user_allocation.level_three,
                                                    tbl_user_allocation.allo_user
                                                    FROM
                                                    tbl_user_allocation
                                                    WHERE
                                                    tbl_user_allocation.allo_user=".session('userid'));  
         if(!empty($getlvlthrids[0]->level_three))
         {
            $lvlthrid_list = $getlvlthrids[0]->level_three;
            $getlvlthr =DB::select("SELECT   tbl_levelthr.id,
                                            tbl_levelthr.name,
                                            tbl_levelthr.status
                                            FROM
                                            tbl_levelthr
                                            WHERE
                                            tbl_levelthr.id iN ($lvlthrid_list)");  
         }


        return view('Tickets')
        ->with('lvlone',$getlvlone)
        ->with('lvltwo',$getlvltwo)
        ->with('lvlthr',$getlvlthr)
        ->with('user',(new ContactController())->retrieveContactById($_GET['contactid']));
	
    }
    public function viewaddticketWhatsappNum($contact_id ,$refNo){
        
        Util::log("Add Ticket","View");

        return view('AddTicketbyWhatsappNo')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('refNo',$refNo)
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }

    public function viewaddticketWebChat($contact_id ,$refNo){
        
        Util::log("Add Ticket","View");

        return view('AddTicketbyWebChatNo')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('refNo',$refNo)
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }

    public function webchatsrvinsertTicket(\Illuminate\Http\Request $request){

        Util::log("Add Ticket","Add");
        $user=session('userid');
        $ticket_num = (new TicketController())->generateticketnumber();
        $contact_id =$request->input('contact_id');
        $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
        //dd($getcontactdata);
         $lvlthr_id = $request->input('hlevel3');
         //dd($lvlthr_id);
        //exit();
        $getfirstUser = DB::select("SELECT Count(tbl_ticket_master.id) AS count, tbl_ticket_master.id, tbl_ticket_master.lvlthr_id, tbl_ticket_master.tktUser, tbl_agent_allocation.allo_agent_id as user_id
            FROM tbl_agent_allocation
            LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
            WHERE tbl_agent_allocation.lvlthr_id = '$lvlthr_id' 
            GROUP BY tbl_ticket_master.tktUser 
            ORDER BY count ASC limit 1"
        );
        if ($getfirstUser) {
            $ticket_user = $getfirstUser[0]->user_id;
        }else{

            $ticket_user = '';
        }
        
        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        
        if($ticketcap=="Call Center"){
            $call_uid = "105.125";
        }
        $kpilevel_id = $request->input('hkpi_level');
        $getkpiinfo = DB::select("SELECT tbl_kpilevel_info.*
            FROM tbl_kpilevel_info
            WHERE tbl_kpilevel_info.id = '$kpilevel_id'
            limit 1"
        );
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId([

            'ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
           ]
        );
        $user_id=$request->input('user_id');
        $get_username=DB::select("SELECT user_master.id, user_master.username
            FROM user_master
            WHERE user_master.id = '$user_id'"
        );

        $refNo = $request->input('refNo');
        DB::table('tbl_srvchat_mst')
        ->where('ch_RefNo',$refNo)
        ->update(['t_status'=>"Ticket Created",
            'chat_Status' => "Closed",
            'description'=>"Ticket No -".$ticket_num." Created By User - ".$get_username[0]->username." At - ".Carbon::now(),
            'update_datetime'=>Carbon::now(),
            'update_userid'=>$user,
            'ticket_id'=>$ticket_id
        ]);
        
        DB::table('tbl_ticket_comment')->insert([

            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
        ]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
        ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
        ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
        ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
        ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
        ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
        ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
        ->where('tbl_ticket_master.id', $ticket_id)
        ->first([
            'tbl_ticket_master.*',
            'csp_contact_master.title',
            'csp_contact_master.firstname',
            'csp_contact_master.lastname',
            'tbl_kpilevel_info.kpilevel_name',
            'user_master.username',
            'csp_contact_master.contact_number',
            'tbl_levelone.name AS lvlone_name',
            'tbl_leveltwo.name AS lvltwo_name',
            'tbl_levelthr.name AS lvlthr_name'
        ]);

        // dd($ticket_details);
        return compact('ticket_details');
    
    }

    public function wacsrvinsertTicket(\Illuminate\Http\Request $request){

        Util::log("Add Ticket","Add");
        $user=session('userid');
        $ticket_num = (new TicketController())->generateticketnumber();
        $contact_id =$request->input('contact_id');
        $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
        //dd($getcontactdata);
         $lvlthr_id = $request->input('hlevel3');
         //dd($lvlthr_id);
        //exit();
        $getfirstUser = DB::select("SELECT Count(tbl_ticket_master.id) AS count, tbl_ticket_master.id, tbl_ticket_master.lvlthr_id, tbl_ticket_master.tktUser, tbl_agent_allocation.allo_agent_id as user_id
            FROM tbl_agent_allocation
            LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
            WHERE tbl_agent_allocation.lvlthr_id = '$lvlthr_id' 
            GROUP BY tbl_ticket_master.tktUser 
            ORDER BY count ASC limit 1"
        );
        if ($getfirstUser) {
            $ticket_user = $getfirstUser[0]->user_id;
        }else{

            $ticket_user = '';
        }
        
        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        
        if($ticketcap=="Call Center"){
            $call_uid = "105.125";
        }
        $kpilevel_id = $request->input('hkpi_level');
        $getkpiinfo = DB::select("SELECT tbl_kpilevel_info.*
            FROM tbl_kpilevel_info
            WHERE tbl_kpilevel_info.id = '$kpilevel_id'
            limit 1"
        );
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId([

            'ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
           ]
        );
        $user_id=$request->input('user_id');
        $get_username=DB::select("SELECT user_master.id, user_master.username
            FROM user_master
            WHERE user_master.id = '$user_id'"
        );

        $refNo = $request->input('refNo');
        DB::table('tbl_srvwac_mst')
        ->where('w_RefNo',$refNo)
        ->update(['t_status'=>"Ticket Created",
            'w_Status' => "Closed",
            'description'=>"Ticket No -".$ticket_num." Created By User - ".$get_username[0]->username." At - ".Carbon::now(),
            'update_datetime'=>Carbon::now(),
            'update_userid'=>$user,
            'ticket_id'=>$ticket_id
        ]);
        
        DB::table('tbl_ticket_comment')->insert([

            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
        ]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
        ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
        ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
        ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
        ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
        ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
        ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
        ->where('tbl_ticket_master.id', $ticket_id)
        ->first([
            'tbl_ticket_master.*',
            'csp_contact_master.title',
            'csp_contact_master.firstname',
            'csp_contact_master.lastname',
            'tbl_kpilevel_info.kpilevel_name',
            'user_master.username',
            'csp_contact_master.contact_number',
            'tbl_levelone.name AS lvlone_name',
            'tbl_leveltwo.name AS lvltwo_name',
            'tbl_levelthr.name AS lvlthr_name'
        ]);

        // dd($ticket_details);
        return compact('ticket_details');
    
    }
    public function viewexcelTicket($id){
	
        if(Util::isAuthorized("getexcelticket")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("getexcelticket")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Ticket Details","View");

        $ticket=$this->retrieveTicketById($id);
        $comments=$this->retrieveTicketComments($id);
        return view('getexcelTicket')
            ->with('ticket',$ticket)
            ->with('comments',$comments)
            ->with('updatepermission',Util::isAuthorized("UpdateTicketStatus"))
            ->with('checkStatus',$this->checkTicketWiseStatus($id));
    }
    public function viewTicket($id){
        if(Util::isAuthorized("ticket")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("ticket")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Ticket Details","View");

        $ticket=$this->retrieveTicketById($id);
        $comments=$this->retrieveTicketComments($id);
       // $userTypeId=DB::select("SELECT user_master.user_type_id FROM user_master WHERE user_master.id =".session('userid'));
      //  dd($userTypeId);
        $userTypeId =session('usertypeid');
        //dd($userTypeId);
        $levelThreeSup=DB::select("SELECT tbl_user_allocation.level_three FROM tbl_user_allocation WHERE allo_user =".session('userid'));
        return view('Ticket')
            ->with('ticket',$ticket)->with('userTypeId',$userTypeId)
            ->with('comments',$comments)
            ->with('updatepermission',Util::isAuthorized("UpdateTicketStatus"))
            ->with('checkStatus',$this->checkTicketWiseStatus($id))
			->with('thirdLevelSup',$levelThreeSup);
    }


     public function checkTicketWiseStatus($id){
	$selectMaxDate = DB::select("SELECT * FROM tbl_ticket_master INNER JOIN tbl_ticket_comment ON tbl_ticket_comment.ticket_id = tbl_ticket_master.id ORDER BY tbl_ticket_comment.date_time DESC LIMIT 1");
	if(!empty($selectMaxDate)){
	return DB::select("SELECT * FROM tbl_ticket_comment WHERE tbl_ticket_comment.returnStatus='Return' AND tbl_ticket_comment.date_time='".$selectMaxDate[0]->date_time."'");
    }
    }

    public function searchTickets1 (){
	 $condition = array();
        $condition1 = array();
        $condition2 = array();
    

//        $lvlone_id = Request::query('lvlone_id');
//
//        if(Request::query('lvlone_id')!=-1){
//            $condition['tbl_ticket_master.lvlone_id'] = $lvlone_id;
//        }


        $status= Request::query('status');
        if(Request::query('status')!='All'){
            $condition['tbl_ticket_master.status'] = $status;
        }else{
            $condition = array();
        }

        $department= Request::query('lvltwo_id');
        if(Request::query('lvltwo_id')!='All'){
            $condition1['tbl_ticket_master.lvltwo_id'] = $department;
        }else{
            $condition1 = array();
        }

        $subdepartment= Request::query('lvlthr_id');
        if(Request::query('lvlthr_id')!='All'){
            $condition2['tbl_ticket_master.lvlthr_id'] = $subdepartment;
        }else{
            $condition2 = array();
        }

        $data = DB::table('tbl_ticket_master')
            ->select(
                'tbl_ticket_master.*',
                DB::raw("CONCAT(firstname,' ',lastname) AS contact_name"),
                DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->where($condition)
            ->groupby('tbl_ticket_master.id')
            ->get();

        return compact('data',$data);
    }
    

    public function searchreturntickets(){  
	 
            $condition  = array();
            $condition1 = array();
            $condition2 = array();
            $condition3 = array();
            $condition4 =array();
            
            $data=array();
            $ticket_ids=array();
            $x=0;
            $user=session('userid');
      
            
                $lvlone_id = Request::query('lvlone_id');

                if(Request::query('lvlone_id')!="All")
                {
                        $condition3['tbl_ticket_master.lvlone_id'] = $lvlone_id;
                }


                $status= Request::query('status');
                if(Request::query('status')!='All'){
                    $condition['tbl_ticket_master.status'] = $status;
                }else{
                    $condition = array();
                }

                $department= Request::query('lvltwo_id');
                if(Request::query('lvltwo_id')!='All'){
                    $condition1['tbl_ticket_master.lvltwo_id'] = $department;
                }else{
                    $condition1 = array();
                }

                $lvlthr_id= Request::query('lvlthr_id');
                if(Request::query('lvlthr_id')!='All'){
                    $condition2['tbl_ticket_master.lvlthr_id'] = $lvlthr_id;
                }else{
                    $condition2 = array();
                }

                $startdate= Request::query('startdate_val');
                $enddate= Request::query('enddate_val');
                //print_r($condition3);
                $dataq = DB::table('tbl_ticket_master')
                    ->select(
                        'tbl_ticket_master.*',
                        DB::raw('(select SUM(TIMESTAMPDIFF(hour, on_hold_start, on_hold_end)) 
                        AS tot_on_hold from tbl_onhold_operation where tbl_ticket_master.id  =   tbl_onhold_operation.ticket_id 
                        group by ticket_id) as tot_on_hold'),
                        'tbl_ticket_master.date_time AS createddate',
                        'tbl_ticket_master.id AS tot_hours',
                        DB::raw("CONCAT(csp_contact_master.title,'.',csp_contact_master.firstname,' ',csp_contact_master.lastname) AS contact_name"),
                        DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                        'tbl_levelone.name AS lvlone_name',
                        'tbl_leveltwo.name AS lvltwo_name',
                        'tbl_levelthr.name AS lvlthr_name')
                    ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
                    ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
                    ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                    ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
                    ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                    ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
                    ->where('tbl_ticket_comment.returnStatus','Return')
                    ->Where('tbl_ticket_master.returnstatus',"1");
                    
                    
                        
                        $dataq = $dataq ->where(function($query) {
                            $user=session('userid');
                            $allocated_lvlone =  DB::table('tbl_user_allocation')
                                        ->where('allo_user', $user)
                                        ->select('level_one','level_two','level_three')->get();

                            if(!$allocated_lvlone->isEmpty())
                            {
                                $all_lvlones = explode(",",$allocated_lvlone[0]->level_one);
                                $all_lvltwos = explode(",",$allocated_lvlone[0]->level_two);
                                $all_lvlthrs = explode(",",$allocated_lvlone[0]->level_three);
                            }else
                            {   
                                $all_lvlones=array();
                                $all_lvltwos=array();
                                $all_lvlthrs=array();
                            }
                            
                            $query->wherein('tbl_ticket_master.lvlone_id',$all_lvlones)
                                    ->orWherein('tbl_ticket_master.lvltwo_id', $all_lvltwos)
                                    ->orWherein('tbl_ticket_master.lvlthr_id', $all_lvlthrs);
                            //$query->where('csp_ticket_master.user_id', '=', session('userid'))
                           
                                });
                      
                  
                /*if(!empty($condition)){
                    $dataq = $dataq->where($condition);
                }*/
                if(!empty($condition1)){
                    $dataq = $dataq->where($condition1);
                }
                if(!empty($condition2)){
                    $dataq = $dataq->where($condition2);
                }
            if(!empty($condition3)){
                    $dataq = $dataq->where($condition3);
                }
            /*if(!empty($condition4)){
                    $dataq = $dataq->where($condition4);
                }*/
                $dataq = $dataq ->whereBetween('tbl_ticket_master.date_time', array($startdate, $enddate));
        
        
                $data=  $dataq =  $dataq->groupby('tbl_ticket_master.id')
                            ->get();
                            foreach($data as $value)
                            {
                            
                               $tot_hours= $this->kpiholidaycal($value->date_time);
                               $data_arr = json_decode(json_encode($data), true); 
                               $key = array_search($value->ticket_num, array_column($data_arr, 'ticket_num'));
                               $data[$key]->tot_hours = $tot_hours;
                            
                            }
 						$ipaddress = (new UsersController())->get_client_ip();
                        $username=session()->get('username');
                        Util::user_auth_log($ipaddress,"User Search Reallocations",$username,"Search Reallocations");
                    
                    return compact('data',$data);
            
       }
    
       public function checkholiday(){
        $rem_datetime = $_GET['rem_datetime'];
       
        $com_holidays = DB::table('com_holidays')
                    ->where('com_holidays.Date', $rem_datetime)
                    ->select('com_holidays.*')->get();                           
       
         return $com_holidays;

        }
       public function kpiholidaycal($cre_datetime)
       {
           //$cre_datetime = $_GET['cre_datetime'];
          
           $working_hours =  DB::select("SELECT
                                           working_hours.*
                                           FROM
                                           working_hours");
       
           $working_hours_arr = json_decode(json_encode($working_hours), true); 
           //dd($myArray);
   
       
           $tot_cre_time = strtotime($cre_datetime);
           $cre_time = date("H:i:s",$tot_cre_time);
           $cre_date = date("Y-m-d",$tot_cre_time);
           $cre_dayname = date('l', strtotime($cre_date));
           $key = array_search($cre_dayname, array_column($working_hours_arr, 'days'));
           $cre_day_endtime = $working_hours_arr[$key]['end_time'];
           
           
           
           
           $date_from = strtotime($cre_date);
           $date_to = strtotime(date("Y-m-d")); 
           
   
           $current_datetime = date("Y-m-d H:i:s");
           
           $cur_time = date("H:i:s");
           $cur_dayname = date('l', strtotime($date_to));
   
           $key = array_search($cur_dayname, array_column($working_hours_arr, 'days'));
           $cur_day_starttime = $working_hours_arr[$key]['start_time'];
           
           
          // echo $cur_noofhours."<br>";
   
           $com_holidays =  DB::select("SELECT
                                           com_holidays.Date
                                           FROM
                                           com_holidays
                                           WHERE
                                           com_holidays.Date BETWEEN $cre_date AND $date_to ");
                                           
           $com_holidays_arr = json_decode(json_encode($com_holidays), true);                   
           $fullday_hours = 0;
   
           for ($i = $date_from+86400; $i <= $date_to-86400; $i+=86400) {
              
             
               if(in_array(date("Y-m-d", $i),array_column($com_holidays_arr, 'Date')))
               {
               
                  
               }else
               {
                   
                   $fullday = date("Y-m-d", $i);
                   $fullday_name = date('l', strtotime($fullday));
                   //echo $fullday_name;
                   $key = array_search($fullday_name, array_column($working_hours_arr, 'days'));
                   $noofhours = $working_hours_arr[$key]['noofhours'];
                   $fullday_hours+=$noofhours;
                   //print_r($working_hours_arr[$key]);
                  // echo "<br>";
               }
               
           }
           $cur_date = date("Y-m-d");
           if($cre_date<$cur_date)
           {
               $cre_noofhours =round((strtotime($cre_day_endtime)-strtotime($cre_time))/3600);
               $cur_noofhours =round((strtotime($cur_time)-strtotime($cur_day_starttime))/3600);
               
           }else
           {
               $cre_noofhours =round((strtotime($cur_time)-strtotime($cre_time))/3600);
               $cur_noofhours =0;
               
           }
          // print_r($cre_noofhours."<br>");
           $tot_hours = $fullday_hours+$cur_noofhours+$cre_noofhours;
           return $tot_hours;
          
       }
    public function insertTicket_old(\Illuminate\Http\Request $request){
        Util::log("Add Ticket","Add");
        $ticket_num = (new TicketController())->generateticketnumber();
        //echo $ticket_num;
        //exit;

        $lvlthr_id = $request->input('hlevel3');
        $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '$lvlthr_id'
                                    GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;
        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
        $user=session('userid');
		$syncNo=$request->input('syncNo');
		$primaryNumber=$request->input('contact_number');
		$selectPrimaryContactAlreadyExit=DB::select("SELECT Count(csp_contact_master.id) AS totcount, csp_contact_master.id FROM csp_contact_master WHERE csp_contact_master.primary_contact = $primaryNumber
");
        if (isset($_POST['lang']) && $_POST['lang'] == 'sinhala') {
        $langS=1;
        } else {
        $langS=0;
        }
        if (isset($_POST['lang']) && $_POST['lang'] == 'tamil') {
        $langT=1;
        } else {
        $langT=0;
        }
        if (isset($_POST['lang']) && $_POST['lang'] == 'english') {
        $langE=1;
        } else {
        $langE=0;
        }
        if($syncNo==""){
            $syncData=0;
        }else{
            $syncData=1;
        }
if($selectPrimaryContactAlreadyExit[0]->totcount==0){
        $contact_id = DB::table('csp_contact_master')->insertGetId(
            ['created_date'=>Carbon::now(),
            'title'=>$request->input('title'),
            'firstname'=>$request->input('firstname'),
            'lastname'=>$request->input('lastname'),
            'address_line1'=>$request->input('address_line1'),
            'address_line2'=>$request->input('address_line2'),
            'city_state_province'=>$request->input('city_state_province'),
            'zip'=>$request->input('zip'),
            'email'=>$request->input('email'),
            'mobile_number'=>$request->input('mobile_number'),
            'primary_contact'=>$request->input('contact_number'),
			'secondary_contact'=>$request->input('secondary_contact'),
            'contact_type_id'=>1,
            'insertUser'=>$user,
            'mediumS' =>$langS,
            'mediumT' =>$langT,
            'mediumE' =>$langE,
            'thirdpartySyncData' =>$syncData,
            'remark'=>$request->input('remark')
            ]
        );
}else{
	$contact_id=$selectPrimaryContactAlreadyExit[0]->id;
	$update= DB::table('csp_contact_master')->where('primary_contact',$primaryNumber)
                    ->update(
                        ['modified_date'=>Carbon::now(),
            'title'=>$request->input('title'),
            'firstname'=>$request->input('firstname'),
            'lastname'=>$request->input('lastname'),
            'address_line1'=>$request->input('address_line1'),
            'address_line2'=>$request->input('address_line2'),
            'city_state_province'=>$request->input('city_state_province'),
            'zip'=>$request->input('zip'),
            'email'=>$request->input('email'),
            'mobile_number'=>$request->input('mobile_number'),
            'primary_contact'=>$request->input('contact_number'),
			'secondary_contact'=>$request->input('secondary_contact'),
            'contact_type_id'=>1,
            'modified_user'=>$user,
            'mediumS' =>$langS,
            'mediumT' =>$langT,
            'mediumE' =>$langE,
			'thirdpartySyncData' =>1,
		'remark'=>$request->input('remark')
            ]
                        );
}



if($syncNo==""){
	$selectPrimaryContactAlreadyExit=DB::select("SELECT Count(csp_contact_master.id) AS totcount, csp_contact_master.id FROM csp_contact_master WHERE csp_contact_master.primary_contact = $primaryNumber
");
$contact_id=$selectPrimaryContactAlreadyExit[0]->id;
           $selectNewInsertData=DB::select("SELECT CONCAT_WS(' ',title, firstname,lastname) AS name, csp_contact_master.primary_contact, csp_contact_master.secondary_contact,
csp_contact_master.email, CONCAT_WS(', ',address_line1, address_line2,city_state_province,zip,country) AS location, csp_contact_master.mobile_number,csp_contact_master.contact_number,
csp_contact_master.nic, csp_contact_master.mediumS,user_master.username, csp_contact_master.mediumT, csp_contact_master.mediumE, csp_contact_master.thirdpartySyncData, csp_contact_master.remark FROM csp_contact_master INNER JOIN user_master ON csp_contact_master.insertUser = user_master.id WHERE primary_contact=$primaryNumber AND csp_contact_master.id=$contact_id");
		   $crmNo = '0'.$selectNewInsertData[0]->primary_contact;
		   $name = $selectNewInsertData[0]->name;
		   $address = $selectNewInsertData[0]->location;
		   $nic = $selectNewInsertData[0]->nic;
		   $eMail = $selectNewInsertData[0]->email;
		   $remark = $selectNewInsertData[0]->remark;
		   $mobileNo = '0'.$selectNewInsertData[0]->mobile_number;
		   $phoneNos = '0'.$selectNewInsertData[0]->secondary_contact;
		   $insertBy = '0'.$selectNewInsertData[0]->username;
		   $language="";
		   if($selectNewInsertData[0]->mediumS==1){
			   $language="SINHALA";
		   }else if($selectNewInsertData[0]->mediumT==1){
			   $language="TAMIL";
		   }else{
			   $language="ENGLISH";
		   }
//API Url
$url = 'http://172.16.2.252:8082/limsy-api/restServices/crmController/registerCustomer';

//Initiate cURL.
$ch = curl_init($url);

//The JSON data.
$jsonData = array(
  "cRMNo" => $crmNo,
  "name" => $name,
  "address" =>$address,
  "nICNo" =>$nic,
  "eMail" =>$eMail,
  "mobileNo" =>$mobileNo,
  "phoneNos"=>$phoneNos,
  "remarks"=>"$remark",
  "nextFollowDate"=>"",
  "insertBy"=>$insertBy,
  "medium"=>$language,
);

//Encode the array into JSON.
$jsonDataEncoded = json_encode($jsonData);
//print_r($jsonDataEncoded);
//Tell cURL that we want to send a POST request.
//curl_setopt($ch, CURLOPT_POST, 1);

//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

//Execute the request
$result = curl_exec($ch);
 //  print_r($jsonDataEncoded);           
//if()
$update= DB::table('csp_contact_master')->where('id',$selectPrimaryContactAlreadyExit[0]->id)
                    ->update(
                        ['thirdpartySyncData'=>1]
                        );
}
//exit();
   $selectPrimaryContactAlreadyExit=DB::select("SELECT Count(csp_contact_master.id) AS totcount, csp_contact_master.id FROM csp_contact_master WHERE csp_contact_master.primary_contact = $primaryNumber
");
$contact_id=$selectPrimaryContactAlreadyExit[0]->id;
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['date_time'=>Carbon::now(),
          //   'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
           //  'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'contact_id'=>$contact_id,
             'user_id'=>$request->input('user_id'),
         //    'lvlone_id'=>$request->input('category'),
             'lvlone_id'=>$request->input('hlevel1'),
             'remark'=>$request->input('remark'),
             'status'=>"Fresh",
             'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'title'=>$request->input('title'),
            'firstname'=>$request->input('firstname'),
            'lastname'=>$request->input('lastname'),
            'address'=>$request->input('address_line1')." ".$request->input('address_line2')." ".$request->input('city_state_province')." ".$request->input('zip'),
            'email'=>$request->input('email'),
            'mobile'=>$request->input('mobile_number'),
            'primarynumber'=>$request->input('primary_contact'),
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		]
        );
       
        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
          //   'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
           //  'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
         //    'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remark'),
             'status'=>"Fresh"
		]
        );
        return Redirect::back();
    }
    
    public function insertTicketmanual(\Illuminate\Http\Request $request){
        Util::log("Add Ticket","Add");
    $ticket_num = (new TicketController())->generateticketnumber();
    $contact_id =$request->input('contact_id');
    $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
    //dd($getcontactdata);
    $lvlthr_id = $request->input('hlevel3');
    $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '$lvlthr_id'
                                    GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;

        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
      $kpilevel_id = $request->input('hkpi_level');
      $getkpiinfo = DB::select("SELECT
                                      tbl_kpilevel_info.*
                                      FROM
                                      tbl_kpilevel_info
                                      WHERE
                                      tbl_kpilevel_info.id = '$kpilevel_id'
                                      limit 1");
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		   ]
        );

        $data=array();
        $data[0] =$ticket_num;
        $data[1] =$ticket_user;  
        //dd($data);
        /*$sendEmails=Mail::send('welcome', $data, function ($message) use ($data) {
        
         $ticket_userid = $data[1];   
            $get_email = DB::select("SELECT
                                    user_master.id,
                                    user_master.email
                                    FROM
                                    user_master
                                    WHERE
                                    user_master.id = '$ticket_userid'");

            $get_content = DB::select("SELECT
                                        tbl_prefix_notification.*
                                        FROM
                                        tbl_prefix_notification
                                        WHERE
                                        tbl_prefix_notification.name = 'new_ticket'");

            $email_address = $get_email[0]->email;
            $ticket_number = $data[0];
            $content = $get_content[0]->content;
            $MailBody="";
            $MailBody .= $content;
            $MailBody .= "<p>Ticket Number - $ticket_number</p><br>";
            $MailBody .= $get_content[0]->greet_content;

            $message->setBody($MailBody, 'text/html');
            $message->from($get_content[0]->from);
            $message->subject($get_content[0]->subject);
            //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
            //$message->to($getEmailBody[0]->claimFormSendAddress);
            //$message->cc('badra@iphonik.com', 'Nadeesha');
            $message->to("krvinodlakshan@gmail.com");
                 
        });*/
        
        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
		]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
        ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
        ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
        ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
        ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
        ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
        ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
        ->where('tbl_ticket_master.id', $ticket_id)
        ->first([
            'tbl_ticket_master.*',
            'csp_contact_master.title',
            'csp_contact_master.firstname',
            'csp_contact_master.lastname',
            'tbl_kpilevel_info.kpilevel_name',
            'user_master.username',
            'csp_contact_master.contact_number',
            'tbl_levelone.name AS lvlone_name',
            'tbl_leveltwo.name AS lvltwo_name',
            'tbl_levelthr.name AS lvlthr_name'
        ]);
				$ipaddress = (new UsersController())->get_client_ip();
                $username=session()->get('username');
                Util::user_auth_log($ipaddress,"User Add Ticket",$username,"Add Ticket");
        return compact('ticket_details');
    }

    public function emailsrvinsertTicket(\Illuminate\Http\Request $request){
        Util::log("Add Ticket","Add");
        $user=session('userid');
        $ticket_num = (new TicketController())->generateticketnumber();
        $contact_id =$request->input('contact_id');
        $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
        //dd($getcontactdata);
        $lvlthr_id = $request->input('hlevel3');
        $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '$lvlthr_id'
                                    GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;

        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
        $kpilevel_id = $request->input('hkpi_level');
        $getkpiinfo = DB::select("SELECT
                                      tbl_kpilevel_info.*
                                      FROM
                                      tbl_kpilevel_info
                                      WHERE
                                      tbl_kpilevel_info.id = '$kpilevel_id'
                                      limit 1");
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		   ]
        );
        $user_id=$request->input('user_id');
        $get_username=DB::select("SELECT
                                    user_master.id,
                                    user_master.username
                                    FROM
                                    user_master
                                    WHERE
                                    user_master.id = '$user_id'");
        $e_id = $request->input('e_id');
                    DB::table('tbl_emlsrv_receive')
                        ->where('id',$e_id)
                        ->update(['status'=>"Ticket Created",
                                 'description'=>"Ticket No -".$ticket_num." Created By User - ".$get_username[0]->username." At - ".Carbon::now(),
                                 'update_datetime'=>Carbon::now(),
                                 'update_userid'=>$user,
                                 'ticket_id'=>$ticket_id]);
        $data=array();
        $data[0] =$ticket_num;
        $data[1] =$ticket_user;  
        //dd($data);
        try 
        {                  
            $sendEmails=Mail::send([], [], function ($message) use ($data) 
            {
        
                $ticket_userid = $data[1];   
                $get_email = DB::select("SELECT
                                            user_master.id,
                                            user_master.email
                                            FROM
                                            user_master
                                            WHERE
                                            user_master.id = '$ticket_userid'");
        
                $get_content = DB::select("SELECT
                                                tbl_prefix_notification.*
                                                FROM
                                                tbl_prefix_notification
                                                WHERE
                                                tbl_prefix_notification.name = 'new_ticket'");
        
                $email_address = $get_email[0]->email;
                $ticket_number = $data[0];
                $content = $get_content[0]->content;
                $MailBody="";
                $MailBody .= $content;
                $MailBody .= "<p>Ticket Number - $ticket_number</p><br>";
                $MailBody .= $get_content[0]->greet_content;
    
                $message->setBody($MailBody, 'text/html');
                $message->from($get_content[0]->from);
                $message->subject($get_content[0]->subject);
                //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                //$message->to($getEmailBody[0]->claimFormSendAddress);
                //$message->cc('badra@iphonik.com', 'Nadeesha');
                $message->to("$email_address");                   
            });      
        } 
        catch (\Exception $e) 
        {
            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email Send Failed !!!");
        }
        
        
        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
		]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
                                ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
                                ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
                                ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                                ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                                ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
                                ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
                                ->where('tbl_ticket_master.id', $ticket_id)
                                ->first([
                                    'tbl_ticket_master.*',
                                    'csp_contact_master.title',
                                    'csp_contact_master.firstname',
                                    'csp_contact_master.lastname',
                                    'tbl_kpilevel_info.kpilevel_name',
                                    'user_master.username',
                                    'csp_contact_master.contact_number',
                                    'tbl_levelone.name AS lvlone_name',
                                    'tbl_leveltwo.name AS lvltwo_name',
                                    'tbl_levelthr.name AS lvlthr_name'
                                ]);
        return compact('ticket_details');
    }


    public function faxsrvinsertTicket(\Illuminate\Http\Request $request){
        Util::log("Add Ticket","Add");
    $ticket_num = (new TicketController())->generateticketnumber();
    $contact_id =$request->input('contact_id');
    $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
    //dd($getcontactdata);
    $lvlthr_id = $request->input('hlevel3');
    $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '$lvlthr_id'
                                    GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;

        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
      $kpilevel_id = $request->input('hkpi_level');
      $getkpiinfo = DB::select("SELECT
                                      tbl_kpilevel_info.*
                                      FROM
                                      tbl_kpilevel_info
                                      WHERE
                                      tbl_kpilevel_info.id = '$kpilevel_id'
                                      limit 1");
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		   ]
        );
        $user_id=$request->input('user_id');
        $get_username=DB::select("SELECT
                                    user_master.id,
                                    user_master.username
                                    FROM
                                    user_master
                                    WHERE
                                    user_master.id = '$user_id'");
        $e_id = $request->input('e_id');
                    DB::table('tbl_faxsrv_receive')
                        ->where('id',$e_id)
                        ->update(['status'=>"Ticket Created",
                                 'description'=>"Ticket No -".$ticket_num." Created By User - ".$get_username[0]->username." At - ".Carbon::now(),
                                 'update_datetime'=>Carbon::now(),
                                 'ticket_id'=>$ticket_id]);
        $data=array();
        $data[0] =$ticket_num;
        $data[1] =$ticket_user;  
        //dd($data);
        try {
            $sendEmails=Mail::send([], [], function ($message) use ($data) {
        
                $ticket_userid = $data[1];   
                   $get_email = DB::select("SELECT
                                           user_master.id,
                                           user_master.email
                                           FROM
                                           user_master
                                           WHERE
                                           user_master.id = '$ticket_userid'");
       
                   $get_content = DB::select("SELECT
                                               tbl_prefix_notification.*
                                               FROM
                                               tbl_prefix_notification
                                               WHERE
                                               tbl_prefix_notification.name = 'new_ticket'");
       
                   $email_address = $get_email[0]->email;
                   $ticket_number = $data[0];
                   $content = $get_content[0]->content;
                   $MailBody="";
                   $MailBody .= $content;
                   $MailBody .= "<p>Ticket Number - $ticket_number</p><br>";
                   $MailBody .= $get_content[0]->greet_content;
       
                   $message->setBody($MailBody, 'text/html');
                   $message->from($get_content[0]->from);
                   $message->subject($get_content[0]->subject);
                   //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                   //$message->to($getEmailBody[0]->claimFormSendAddress);
                   //$message->cc('badra@iphonik.com', 'Nadeesha');
                   $message->to("$email_address");
                        
               });         
        } 
        catch (\Exception $e) {
                $ipaddress = (new UsersController())->get_client_ip();
                $username=session()->get('username');
                Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email Send Failed !!!");
                // return "false2"; 
        }
        
        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
		]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
        ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
        ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
        ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
        ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
        ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
        ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
        ->where('tbl_ticket_master.id', $ticket_id)
        ->first([
            'tbl_ticket_master.*',
            'csp_contact_master.title',
            'csp_contact_master.firstname',
            'csp_contact_master.lastname',
            'tbl_kpilevel_info.kpilevel_name',
            'user_master.username',
            'csp_contact_master.contact_number',
            'tbl_levelone.name AS lvlone_name',
            'tbl_leveltwo.name AS lvltwo_name',
            'tbl_levelthr.name AS lvlthr_name'
        ]);
        return compact('ticket_details');
    }

    public function smssrvinsertTicket(\Illuminate\Http\Request $request){
        Util::log("Add Ticket","Add");
    $ticket_num = (new TicketController())->generateticketnumber();
    $contact_id =$request->input('contact_id');
    $getcontactdata=DB::select('SELECT csp_contact_master.* FROM csp_contact_master Where csp_contact_master.id='.$contact_id);
    //dd($getcontactdata);
    $lvlthr_id = $request->input('hlevel3');
    $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '$lvlthr_id'
                                    GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;

        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
      $kpilevel_id = $request->input('hkpi_level');
      $getkpiinfo = DB::select("SELECT
                                      tbl_kpilevel_info.*
                                      FROM
                                      tbl_kpilevel_info
                                      WHERE
                                      tbl_kpilevel_info.id = '$kpilevel_id'
                                      limit 1");
        $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
            //'lvltwo_id'=>$request->input('department'),
            'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
            'lvlthr_id'=>$request->input('hlevel3'),
            'contact_id'=>$request->input('contact_id'),
            'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
            'lvlone_id'=>$request->input('hlevel1'),
            'remark'=>$request->input('remarkTicket'),
            'status'=>"Fresh",
            'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$getcontactdata[0]->title,
            'firstname'=>$getcontactdata[0]->firstname,
            'lastname'=>$getcontactdata[0]->lastname,
            'address'=>$getcontactdata[0]->address_line1." ".$getcontactdata[0]->address_line2." ".$getcontactdata[0]->city_state_province." ".$getcontactdata[0]->zip,
            'email'=>$getcontactdata[0]->email,
            'secondary_contact'=>$getcontactdata[0]->secondary_contact,
            'secondary_contact2'=>$getcontactdata[0]->secondary_contact2,
            'primary_contact'=>$getcontactdata[0]->primary_contact,
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		   ]
        );
        $user_id=$request->input('user_id');
        $get_username=DB::select("SELECT
                                    user_master.id,
                                    user_master.username
                                    FROM
                                    user_master
                                    WHERE
                                    user_master.id = '$user_id'");
        $e_id = $request->input('e_id');
                    DB::table('tbl_smssrv_receive')
                        ->where('id',$e_id)
                        ->update(['status'=>"Ticket Created",
                                 'description'=>"Ticket No -".$ticket_num." Created By User - ".$get_username[0]->username." At - ".Carbon::now(),
                                 'update_datetime'=>Carbon::now(),
                                 'ticket_id'=>$ticket_id]);
        $data=array();
        $data[0] =$ticket_num;
        $data[1] =$ticket_user;  
        //dd($data);
       try {
            $sendEmails=Mail::send([], [], function ($message) use ($data) {
        
                $ticket_userid = $data[1];   
                   $get_email = DB::select("SELECT
                                           user_master.id,
                                           user_master.email
                                           FROM
                                           user_master
                                           WHERE
                                           user_master.id = '$ticket_userid'");
       
                   $get_content = DB::select("SELECT
                                               tbl_prefix_notification.*
                                               FROM
                                               tbl_prefix_notification
                                               WHERE
                                               tbl_prefix_notification.name = 'new_ticket'");
       
                   $email_address = $get_email[0]->email;
                   $ticket_number = $data[0];
                   $content = $get_content[0]->content;
                   $MailBody="";
                   $MailBody .= $content;
                   $MailBody .= "<p>Ticket Number - $ticket_number</p><br>";
                   $MailBody .= $get_content[0]->greet_content;
       
                   $message->setBody($MailBody, 'text/html');
                   $message->from($get_content[0]->from);
                   $message->subject($get_content[0]->subject);
                   //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                   //$message->to($getEmailBody[0]->claimFormSendAddress);
                   //$message->cc('badra@iphonik.com', 'Nadeesha');
                   $message->to("$email_address");
                        
               });
                
        } 
        catch (\Exception $e) {
                $ipaddress = (new UsersController())->get_client_ip();
                $username=session()->get('username');
                Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email Send Failed !!!");
                //  return "false2"; 
        }

        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
		]
        );
       
        $ticket_details = DB::table('tbl_ticket_master')
        ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
        ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
        ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
        ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
        ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
        ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
        ->where('tbl_ticket_master.id', $ticket_id)
        ->first([
            'tbl_ticket_master.*',
            'csp_contact_master.title',
            'csp_contact_master.firstname',
            'csp_contact_master.lastname',
            'tbl_kpilevel_info.kpilevel_name',
            'user_master.username',
            'csp_contact_master.contact_number',
            'tbl_levelone.name AS lvlone_name',
            'tbl_leveltwo.name AS lvltwo_name',
            'tbl_levelthr.name AS lvlthr_name'
        ]);
        return compact('ticket_details');
    }



    public function generateticketnumber(){
        //$last_ticket_num = tbl_ticket_master::orderBy('id', 'desc')->first()->ticket_num;
        $getlast_ticket_num = DB::select("SELECT
                                    tbl_ticket_master.ticket_num
                                    FROM
                                    tbl_ticket_master
                                    ORDER BY tbl_ticket_master.id desc limit 1");
        $prefix = "IP"; 
        $pfxlen=strlen($prefix);                           
        
        if(!empty($getlast_ticket_num[0]->ticket_num))
        {
            $last_ticket_num = $getlast_ticket_num[0]->ticket_num;
            $getnumber=  substr($last_ticket_num,$pfxlen);
            $incre_getnumber = intval($getnumber) + 1;; 
            $ticket_num =$prefix.str_pad($incre_getnumber,'6','0',STR_PAD_LEFT);  
        }else
        {
            $ticket_num = $prefix."000001";
        }
        return $ticket_num;
    }

    
    public function ticketdetail_sendbyemail(){
        $ticket_id =  request::input('ticket_id');
// dd($ticket_id);
        $ticket=$this->retrieveTicketById($ticket_id);
         // dd($ticket);
        $data[0] =$ticket->ticket_num;
        $data[1] =$ticket->email; 
         // dd($data[1]);
        if($data[1]=="")
        {
            return "false";
        }else
        {
             //dd($data);
            try {
                $sendEmails=Mail::send([], [], function ($message) use ($data) {
                    $email_address = $data[1];
        
                    $MailBody="";
                    $MailBody .= '<p>Dear Sir/Madam</p><br>';
                    $MailBody .= '<p>As per your latest communication with us we have created a support request (ref-'.$data[0].').</p>';
                    $MailBody .= '<p>Our team will getting touch with you shortly.</p><br>';
                    $MailBody .= '<p>Best regards</p>';
                    $MailBody .= '<p>Iphonik Team</p>';
                    
                    $message->setBody($MailBody, 'text/html');
                    $message->from('ccsystememail@amana.lk', 'IphonikCsp');
                    $message->subject('Ticket Creation - Iphonik Csp');
                    //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                    //$message->to($getEmailBody[0]->claimFormSendAddress);
                    //$message->cc('badra@iphonik.com', 'Nadeesha');
                    $message->to("$email_address");
                         
                    });
                }
            catch (\Exception $e) 
                {
                        $ipaddress = (new UsersController())->get_client_ip();
                        $username=session()->get('username');
                        Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email Send Failed !!!");
                    return "false2";
                }
            return "true";
        } 
            
    }  
            //return view("csv_view_levelone");
        

        public function ticketdetail_sendviasms(){
            $ticket_id =  request::input('ticket_id');
    
            $ticket=$this->retrieveTicketById($ticket_id);
            // dd($ticket);
            $ticket_number =$ticket->ticket_num;
            $mobi =$ticket->primary_contact; 
            if($data[1]=="")
            {
                echo"false";
            }else
            {
    
                //--------------sms notification--------------------------------------
                /*$message='';
                $message .='Dear Customer,\r\nClaim Ref No '.$ticket_number.'\r\n';
                $message .='We regret above claim is rejected due to \r\n';
                $message .= 'Fairfirst Health Claims Dept(0112428190)';
                
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "http://172.24.10.44/restSMSWService/index.php/auth/login",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\r\n\"username\" : \"admin\",\r\n\"password\" : \"Admin123$\"\r\n}",
                CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 5a2c1cc1-5cfe-4c86-b199-6656a1b1203b",
                "auth-key: simplerestapi",
                "client-service: frontend-client"
                ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                echo "cURL Error #:" . $err;
                } else {

                //$response;
                $data = json_decode($response);
                $token=$data->token;
                $id=$data->id;


                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://172.24.10.44/restSMSWService/index.php/SmsApi/create",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "{\r\n\"mobileNo\" : \"$mobi\",\r\n\"message\" : \"$message\",\r\n\"sntUser\" : \"IPHONIK\"\r\n}",
                    CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Postman-Token: fe30e7b1-93ea-43c6-8175-3fde9ececf7f",
                    "auth-key: simplerestapi",
                    "authorization: $token",
                    "client-service: frontend-client",
                    "content-type: application/json",
                    "user-id: $id "
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) 
                {
                echo "cURL Error #:" . $err;
                
                } 
                else 
                {
                //echo $response;
                /*$data = json_decode($response);
                $token=$data->token;
                $id=$data->id;
               
                }
                }*/
                echo"true";
            } 
           
     
                //return view("csv_view_levelone");
            }
    public function insertTicket(\Illuminate\Http\Request $request){
        $ticket_num = (new TicketController())->generateticketnumber();

        $lvlthr_id = $request->input('hlevel3');
         // dd($lvlthr_id);
        $getfirstUser = DB::select("SELECT
                                    Count(tbl_ticket_master.id) AS count,
                                    tbl_ticket_master.id,
                                    tbl_ticket_master.lvlthr_id,
                                    tbl_ticket_master.tktUser,
                                    tbl_agent_allocation.allo_agent_id as user_id
                                    FROM
                                    tbl_agent_allocation
                                    LEFT JOIN tbl_ticket_master ON tbl_agent_allocation.lvlthr_id = tbl_ticket_master.lvlthr_id 
                                    AND tbl_ticket_master.tktUser = tbl_agent_allocation.allo_agent_id
                                    WHERE
                                    tbl_agent_allocation.lvlthr_id = '".$lvlthr_id."' GROUP BY
                                    tbl_ticket_master.tktUser 
                                    ORDER BY count ASC limit 1");

        $ticket_user = $getfirstUser[0]->user_id;
        $ticketcap = $request->input('hticketcapturemode');
        $call_uid = Null;
        if($ticketcap=="Call Centre"){
            $call_uid = "105.125";
        }
        $user=session('userid');
		$syncNo=$request->input('syncNo');
		$primaryNumber=$request->input('contact_number');
		//$selectPrimaryContactAlreadyExit=DB::select("SELECT Count(csp_contact_master.id) AS totcount, csp_contact_master.id FROM csp_contact_master WHERE csp_contact_master.primary_contact = $primaryNumber");
    if (isset($_POST['lang']) && $_POST['lang'] == 'sinhala') {
        $langS=1;
        } else {
        $langS=0;
        }
        if (isset($_POST['lang']) && $_POST['lang'] == 'tamil') {
        $langT=1;
        } else {
        $langT=0;
        }
        if (isset($_POST['lang']) && $_POST['lang'] == 'english') {
        $langE=1;
        } else {
        $langE=0;
        }
        if($syncNo==""){
            $syncData=0;
        }else{
            $syncData=1;
        }    
        $cusid=$request->input('cusid');    
        $update= DB::table('csp_contact_master')->where('id',$cusid)
                    ->update(
                        ['modified_date'=>Carbon::now(),
                        'title'=>$request->input('title'),
                        'firstname'=>$request->input('firstname'),
                        'lastname'=>$request->input('lastname'),
                        'address_line1'=>$request->input('address_line1'),
                        'address_line2'=>$request->input('address_line2'),
                        'city_state_province'=>$request->input('city_state_province'),
                        'zip'=>$request->input('zip'),
                        'email'=>$request->input('email'),
                        'primary_contact'=>$request->input('primary_contact'),
                        'secondary_contact'=>$request->input('secondary_contact'),
                        'contact_type_id'=>1,
                        'modified_user'=>$user,
                        'mediumS' =>$langS,
                        'mediumT' =>$langT,
                        'mediumE' =>$langE,
                        'thirdpartySyncData' =>1,
                        'remark'=>$request->input('cus_remark')]
                        );
    
        $kpilevel_id = $request->input('hkpi_level');
        $getkpiinfo = DB::select("SELECT
                                        tbl_kpilevel_info.*
                                        FROM
                                        tbl_kpilevel_info
                                        WHERE
                                        tbl_kpilevel_info.id = '$kpilevel_id'
                                        limit 1");

       $ticket_id = DB::table('tbl_ticket_master')->insertGetId(
            ['ticket_num'=>$ticket_num,
            'date_time'=>Carbon::now(),
          //   'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
           //  'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'contact_id'=>$cusid,
             'user_id'=>$request->input('user_id'),
         //    'lvlone_id'=>$request->input('category'),
             'lvlone_id'=>$request->input('hlevel1'),
             'remark'=>$request->input('remarkTicket'),
             'status'=>"Fresh",
             'priority'=>"Normal",
            'tktUser'=>$ticket_user,
            'kpilevel_id'=>$request->input('hkpi_level'),
            'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
            'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
            'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
            'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
            'title'=>$request->input('title'),
            'firstname'=>$request->input('firstname'),
            'lastname'=>$request->input('lastname'),
            'address'=>$request->input('address_line1')." ".$request->input('address_line2')." ".$request->input('city_state_province')." ".$request->input('zip'),
            'email'=>$request->input('email'),
            'secondary_contact'=>$request->input('secondary_contact'),
            'secondary_contact2'=>$request->input('secondary_contact2'),
            'primary_contact'=>$request->input('primary_contact'),
            'tickettype'=>$request->input('htickettype'),
            'call_uid'=>$call_uid,
            'ticketcapturemode'=>$request->input('hticketcapturemode'),
            'orderreference'=>$request->input('order_ref'),
            'referenceperson'=>$request->input('order_ref_person')
		]
        );
        DB::table('tbl_ticket_comment')->insert(
            [
            'date_time'=>Carbon::now(),
            'ticket_id'=>$ticket_id,
            //'lvltwo_id'=>$request->input('department'),
             'lvltwo_id'=>$request->input('hlevel2'),
            //'lvlthr_id'=>$request->input('subdepartment'),
             'lvlthr_id'=>$request->input('hlevel3'),
             'user_id'=>$request->input('user_id'),
            //'lvlone_id'=>$request->input('category'),
             'comment'=>"Ticket Created-".$request->input('remarkTicket'),
             'status'=>"Fresh"
		]
        );
            $get_email = DB::select("SELECT
                                    user_master.id,
                                    user_master.email
                                    FROM
                                    user_master
                                    WHERE
                    user_master.id = '$ticket_user'");
        $data=array();
        $data[0] =$ticket_num;
        $data[1] =$ticket_user;
         
        /*if(!empty($get_email[0]->email)){
			$data[2] =$get_email[0]->email;
            //dd($data);
        $sendEmails=Mail::send('welcome', $data, function ($message) use ($data) {

            $ticket_userid = $data[1];   
               
   
            $get_content = DB::select("SELECT
                                        tbl_prefix_notification.*
                                        FROM
                                        tbl_prefix_notification
                                        WHERE
                                        tbl_prefix_notification.name = 'new_ticket'");

               $email_address = $data[2];
            $ticket_number = $data[0];
            $content = $get_content[0]->content;
            $MailBody="";
            $MailBody .= $content;
            $MailBody .= "<p>Ticket Number - $ticket_number</p><br>";
            $MailBody .= $get_content[0]->greet_content;

            $message->setBody($MailBody, 'text/html');
            $message->from($get_content[0]->from);
            $message->subject($get_content[0]->subject);
            //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
            //$message->to($getEmailBody[0]->claimFormSendAddress);
            //$message->cc('badra@iphonik.com', 'Nadeesha');
            $message->to("$email_address");
                 
        });
        
        }*/  
        
       
        $incomingcall=$request->input('incomingcall');
        $sipid=$request->input('sipid');
        $linkedid=$request->input('linkedid');

        
        $get_calllog = DB::table('csp_callhistory')
                    ->select('csp_callhistory.call_datetime','csp_callhistory.call_log')
                    ->where('unq_id',$linkedid)
                    ->first();

        $userid=session('userid');
        if(!empty($get_calllog))
        {
            $callid=$get_calllog->call_datetime;

            $userid=session('userid');
            DB::table('csp_callhistory')
            ->where('unq_id',$linkedid)
            ->where('sipid',$sipid)
            ->update([
                'call_log'=> $get_calllog->call_log." - "."Ticket Created - ".$ticket_num
            ]);
        }
           

       //return Redirect::back();
       $ticket_details = DB::table('tbl_ticket_master')
                    ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
                    ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
                    ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                    ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                    ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
                    ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
                    ->where('tbl_ticket_master.id', $ticket_id)
                    ->first([
                        'tbl_ticket_master.*',
                        'csp_contact_master.title',
                        'csp_contact_master.firstname',
                        'csp_contact_master.lastname',
                        'tbl_kpilevel_info.kpilevel_name',
                        'user_master.username',
                        'csp_contact_master.contact_number',
                        'tbl_levelone.name AS lvlone_name',
                        'tbl_leveltwo.name AS lvltwo_name',
                        'tbl_levelthr.name AS lvlthr_name'
                    ]);
                    $ipaddress = (new UsersController())->get_client_ip();
                    $username=session()->get('username');
                    Util::user_auth_log($ipaddress,"User Create Ticket",$username,"Create ticket");
        			return compact('ticket_details');
       
    }
    public function getticketforcomment(){

       $ticket_id= Request::query('ticket_id');

        $ticket_details = DB::table('tbl_ticket_master')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
            ->where('tbl_ticket_master.id', $ticket_id)
            ->first([
                'tbl_ticket_master.*',
                'csp_contact_master.title',
                'csp_contact_master.firstname',
                'csp_contact_master.lastname',
                'tbl_kpilevel_info.kpilevel_name',
                'user_master.username',
                'csp_contact_master.contact_number',
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name'
            ]);

        return compact('ticket_details');
    }

    public function viewAddTicket($contact_id){
        if(Util::isAuthorized("viewaddticket")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("viewaddticket")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("Add Ticket","View");

		$ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User open the add ticket form-$username",$username,"View Add Ticket Form");
        return view('AddTicket')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }
    public function viewaddticketbyemail($contact_id ,$e_id){
        
        Util::log("Add Ticket","View");

        return view('AddTicketbyemail')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('e_id',$e_id)
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }
    public function viewaddticketbynum($contact_id,$e_id){
        
        Util::log("Add Ticket","View");

        return view('AddTicketbynum')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('e_id',$e_id)
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }

    public function viewaddticketbyfxnum($contact_id,$e_id){
        
        Util::log("Add Ticket","View");

        return view('AddTicketbyfxnum')
            ->with('level1s',(new LevelOneController())->retrievelvlone())
            ->with('e_id',$e_id)
            ->with('contact',DB::table('csp_contact_master')->where('id',$contact_id)->first());
    }
    public function viewTransferTicket($id){
        if(Util::isAuthorized("viewtransferticket")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("viewtransferticket")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("Transfer Ticket","View");

        $getuser = DB::table('user_master')
                    ->select('username','id')
                    ->where('user_type_id','18')  
                    ->get();
    // print_r($getuser);exit();
            $lvloneid_list = "";
            $lvltwoid_list = "";
            $lvlthrid_list = "";
            $getlvlone=array();
            $getlvltwo=array();
            $getlvlthr=array();
            $getlvloneids =DB::select("SELECT   tbl_user_allocation.level_one,
                                                tbl_user_allocation.allo_user
                                                FROM
                                                tbl_user_allocation
                                                WHERE
                                                tbl_user_allocation.allo_user=".session('userid'));
            
            if(!empty($getlvloneids[0]->level_one))
            {
                $lvloneid_list = $getlvloneids[0]->level_one;
                $getlvlone =DB::select("SELECT   tbl_levelone.id,
                                                tbl_levelone.name,
                                                tbl_levelone.status
                                                FROM
                                                tbl_levelone
                                                WHERE
                                                tbl_levelone.id iN ($lvloneid_list)");

            }

            $getlvltwoids =DB::select("SELECT   tbl_user_allocation.level_two,
                                                tbl_user_allocation.allo_user
                                                FROM
                                                tbl_user_allocation
                                                WHERE
                                                tbl_user_allocation.allo_user=".session('userid'));

            if(!empty($getlvltwoids[0]->level_two))
            {
                $lvltwoid_list = $getlvltwoids[0]->level_two;
                $getlvltwo =DB::select("SELECT   tbl_leveltwo.id,
                                                    tbl_leveltwo.name,
                                                    tbl_leveltwo.status
                                                    FROM
                                                    tbl_leveltwo
                                                    WHERE
                                                    tbl_leveltwo.id iN ($lvltwoid_list)");  

            }

            $getlvlthrids =DB::select("SELECT   tbl_user_allocation.level_three,
                                                        tbl_user_allocation.allo_user
                                                        FROM
                                                        tbl_user_allocation
                                                        WHERE
                                                        tbl_user_allocation.allo_user=".session('userid'));  
             if(!empty($getlvlthrids[0]->level_three))
             {
                $lvlthrid_list = $getlvlthrids[0]->level_three;
                $getlvlthr =DB::select("SELECT   tbl_levelthr.id,
                                                tbl_levelthr.name,
                                                tbl_levelthr.status
                                                FROM
                                                tbl_levelthr
                                                WHERE
                                                tbl_levelthr.id iN ($lvlthrid_list)");  
             }


        $ticket = (new TicketController())->retrieveTicketById($id);

        return view('TransferTicket')
            ->with('getuser',$getuser)
            ->with('lvlone',$getlvlone)
            ->with('lvltwo',$getlvltwo)
            ->with('lvlthr',$getlvlthr)
            ->with('ticket',(new TicketController())->retrieveTicketById($id));
    }

    public function transferrableSubDeps(){
        $data= DB::table('tbl_levelthr')
            ->select('*')
            ->whereIn('tbl_levelthr.lvltwo_id',
                    (DB::table('csp_department_user')
                        ->where('user_id',session('userid'))
                        ->pluck('lvltwo_id'))
            )
            ->where('lvltwo_id',$_GET['lvltwo_id'])
            ->get();
        return compact('data');
    }

    public function retrieveTransferableUsers(){
        $departments = DB::table('user_master')
            ->whereIn('user_master.id',
                DB::table('csp_department_user')
                    ->where('lvltwo_id',Request::query('$lvltwo_id'))
                    ->pluck('user_id')
            )
            ->orWhereIn('user_master.id',
                DB::table('csp_subdepartment_user')
                    ->where('lvlthr_id',Request::query('lvlthr_id'))
                    ->pluck('user_id')
            )
            ->get(['user_master.*']);

        dd($departments);

    }

//    public function transferrableUsers(){
//        $data= DB::table('user_master')
//            ->select('*')
//            ->whereIn('user_master.id',
//                    (DB::table('csp_department_user')
//                        ->where('user_id',session('userid'))
//                        ->pluck('lvltwo_id'))
//            )
//            ->where('lvltwo_id',$_GET['lvltwo_id'])
//            ->get();
//        return compact('data');
//    }

public function transferTicket(\Illuminate\Http\Request $request){
	//echo $request->input('status');
	//exit();
        Util::log("View Ticket Details","Transfer");
	if($request->input('status')==='T'){

        $ticket_id = $request->input('ticket_id');
        
        $getdetailsofticket = DB::table('tbl_ticket_master') 
                                ->select(['tbl_ticket_master.*',
                                    'tbl_kpilevel_info.kpilevel_name AS kpilevel_name',
                                    'tbl_levelone.name AS lvlone_name',
                                    'tbl_leveltwo.name AS lvltwo_name',
                                    'tbl_levelthr.name AS lvlthr_name'])    
                                ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
                                ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                                ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                                ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')                                
                                ->where('tbl_ticket_master.id', $ticket_id)
                                ->first();

            if(!empty($getdetailsofticket))
            {
                $kpilevel_id = $request->input('kpilevel_id');
                $getkpiinfo = DB::select("SELECT
                                                tbl_kpilevel_info.*
                                                FROM
                                                tbl_kpilevel_info
                                                WHERE
                                                tbl_kpilevel_info.id = '$kpilevel_id'
                                                limit 1");

                                  
                $update= DB::table('tbl_ticket_master')->where('id',$request->input('ticket_id'))
                    ->update(
                        ['lvlone_id' => $request->input('lvltone_id'),
                        'lvltwo_id' => $request->input('lvltwo_id'),
                        'lvlthr_id' => $request->input('lvlthr'),
                        'status' =>'Work In Progress',
                        'tktUser' => $request->input('user_id'),
                        'kpilevel_id' => $request->input('kpilevel_id'),
                        'kpil_des'=>$getkpiinfo[0]->kpilevel_name,
                        'l1_kpi'=>$getkpiinfo[0]->l1_kpi,
                        'l2_kpi'=>$getkpiinfo[0]->l2_kpi,
                        'l3_kpi'=>$getkpiinfo[0]->l3_kpi,
                        'returnstatus'=>"0"]
                        );
            
                DB::table('tbl_ticket_comment')->insert(
                            ['ticket_id'=>$request->input('ticket_id'),
                            'lvltwo_id'=>$request->input('lvltwo_id'),
                             'lvlthr_id'=>$request->input('lvlthr'),
                             'user_id'=>$request->input('user_id'),
                             'status'=>'Work In Progress',
                             'comment'=>'Ticket Transferred - '.$request->input('comment'),
                             'returnStatus'=>'',
                             'date_time'=>Carbon::now()
                            ]);
            }

            if(!empty($update))
            {

                $getdetailsofticketnew = DB::table('tbl_ticket_master') 
                                    ->select(['tbl_ticket_master.*',
                                        'tbl_kpilevel_info.kpilevel_name AS kpilevel_name',
                                        'tbl_leveltwo.name AS lvltwo_name',
                                        'tbl_levelone.name AS lvlone_name',
                                        'tbl_levelthr.name AS lvlthr_name'])    
                                    ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
                                    ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                                    ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                                    ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')                                
                                    ->where('tbl_ticket_master.id', $ticket_id)
                                    ->first();
                }
       

               
               
        

	    DB::table('tbl_ticket_comment')->insert(
            ['ticket_id'=>$request->input('ticket_id'),
            'lvltwo_id'=>$request->input('lvltwo_id'),
             'lvlthr_id'=>$request->input('lvlthr'),
             'user_id'=>$request->input('user_id'),
             'status'=>'Working Progress',
             'comment'=>"Reallocated ".$getdetailsofticket->ticket_num." location From ".$getdetailsofticket->lvltwo_name." to ".
                $getdetailsofticketnew->lvltwo_name." and Category From ".$getdetailsofticket->lvlthr_name.
                " to ".$getdetailsofticketnew->lvlthr_name." and KPI Level Info From ".
                    $getdetailsofticket->kpilevel_name." to ".$getdetailsofticketnew->kpilevel_name."-".$request->input('comment'),
                'returnStatus'=>'',
             'date_time'=>Carbon::now()
            ]);


            $lvlthr_id = $getdetailsofticketnew->lvlthr_id;
            $getlvlthr_sup = DB::select("SELECT
                                        tbl_user_allocation.allo_user
                                        FROM
                                        tbl_user_allocation
                                        WHERE
                                        tbl_user_allocation.level_three like '%$lvlthr_id%'");
            
            foreach($getlvlthr_sup as $value)
            {
                $get_email = DB::select("SELECT
                                                user_master.id,
                                                user_master.email
                                                FROM
                                                user_master
                                                WHERE
                                                user_master.id = '$value->allo_user'");

                $data=array();
                $data[0] =$getdetailsofticket->ticket_num;
               
                $data[2] =$getdetailsofticket->lvlone_name; 
                $data[3] =$getdetailsofticket->lvltwo_name; 
                $data[4] =$getdetailsofticket->lvlthr_name;   
                if(!empty($get_email)){

                    $data[1] =$get_email[0]->email;
                   /* $sendEmails=Mail::send('welcome', $data, function ($message) use ($data) {
                                    
                        $user_name=DB::table('user_master')
                                    ->where('id',session('userid'))
                                    ->first(['username']);            
                        $email_address = $data[1];

                        $get_content = DB::select("SELECT
                                                        tbl_prefix_notification.*
                                                        FROM
                                                        tbl_prefix_notification
                                                        WHERE
                                                        tbl_prefix_notification.name = 'reallocation'");

                        $content = $get_content[0]->content;
                        $MailBody="";
                        $MailBody .= $content;
                        $MailBody .= '<p>From :'.$data[2].' \ '.$data[3].' \  '.$data[4].'</p>';
                        $MailBody .= '<p>Transfer User - '.$user_name->username.'</p>';
                        $MailBody .= '<p>ticket Id - '.$data[0].'</p>';
                        $MailBody .= $get_content[0]->greet_content;
                        
                        $message->setBody($MailBody, 'text/html');
                        $message->from($get_content[0]->from);
                        $message->subject($get_content[0]->subject);
                        //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                        //$message->to($getEmailBody[0]->claimFormSendAddress);
                        //$message->cc('badra@iphonik.com', 'Nadeesha');
                        $message->to("$email_address");
                            
                    });     */ 
                }
            }

                
                $tktUser = $getdetailsofticketnew->tktUser;
                $email_address=DB::table('user_master')
                                    ->where('id',$tktUser)
                                    ->first(['email']);  

                $data1=array();
                $data1[0] =$getdetailsofticket->ticket_num;
                
                $data1[2] =$getdetailsofticket->lvlone_name; 
                $data1[3] =$getdetailsofticket->lvltwo_name; 
                $data1[4] =$getdetailsofticket->lvlthr_name;  

                if(!empty($email_address))
                {
                    $data1[1] =$email_address->email;
                   /* $sendEmails=Mail::send('welcome', $data1, function ($message) use ($data1) {
                                      
                                     
                       
                        $email_address = $data1[1];
                        $user_name=DB::table('user_master')
                                            ->where('id',session('userid'))
                                            ->first(['username']);  

                        $get_content = DB::select("SELECT
                                            tbl_prefix_notification.*
                                            FROM
                                            tbl_prefix_notification
                                            WHERE
                                            tbl_prefix_notification.name = 'reallocation'");

                        $content = $get_content[0]->content;
                        $MailBody="";
                        $MailBody .= $content;
                        $MailBody .= '<p>From :'.$data1[2].' \ '.$data1[3].' \  '.$data1[4].'</p>';
                        $MailBody .= '<p>Transfer User - '.$user_name->username.'</p>';
                        $MailBody .= '<p>ticket Id - '.$data1[0].'</p>';
                        $MailBody .= $get_content[0]->greet_content;
                        
                        $message->setBody($MailBody, 'text/html');
                        $message->from($get_content[0]->from);
                        $message->subject($get_content[0]->subject);
                        //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                        //$message->to($getEmailBody[0]->claimFormSendAddress);
                        //$message->cc('badra@iphonik.com', 'Nadeesha');
                        $message->to("$email_address");
                            
                    }); */     
                }


        //$this->updateStatus($request);
	    }else{
		$selectDataTicketWise=DB::select("SELECT * FROM tbl_ticket_master WHERE id=".$request->input('ticket_id'));
		DB::table('tbl_ticket_master')->where('id',$request->input('ticket_id'))
            ->update(
                ['status' => 'Cancelled',
                'returnstatus' => '0']
            );
		DB::table('tbl_ticket_comment')->insert(
            ['ticket_id'=>$request->input('ticket_id'),
             'lvltwo_id'=>$selectDataTicketWise[0]->lvltwo_id,
             'lvlthr_id'=>$selectDataTicketWise[0]->lvlthr_id,
             'user_id'=>$selectDataTicketWise[0]->user_id,
             'status'=>'Cancelled',
            'comment'=>'Ticket Cancelled - '.$request->input('cancelcomment'),
            'returnStatus'=>'NULL',
             'date_time'=>Carbon::now()
            ]);
        //$this->updateStatusTransfer($request);
	    
	}
        
	
        return redirect("ticket/".$request->input('ticket_id'));
    }
    

       public function updateStatus(\Illuminate\Http\Request $request){
        // echo $request->input('returnStatus');
        // exit();
        $userid=session('userid');
        // dd($userid);
        Util::log("View Ticket Details","Update Status");
        if (isset($_POST['statusReturn']) && $_POST['statusReturn'] == 'Return') {

            $Return='Return';
            
            } else {
            
            $Return='';
            
            }
             // dd($Return);
        $user_id=DB::table('tbl_ticket_master')
            ->where('id',$request->ticket_id)
            ->first(['user_id']);

            
            if (!file_exists('ticketImage/'.$request->ticket_id)) {
                mkdir('ticketImage/'.$request->ticket_id, 0777, true);
            }
            $uploadPath='ticketImage/'.$request->ticket_id;
            if(isset($_FILES['image'])){
                $file_name = $_FILES['image']['name'];
                $file_tmp = $_FILES['image']['tmp_name'];
                $target = 'ticketImage/'.$request->ticket_id.'/'.$file_name;
                if (empty($errors) == true) {
                    move_uploaded_file($file_tmp, $target);
                } else {
                    print_r($errors);
                }
            }
    
            DB::table('tbl_ticket_comment')->insert(
                ['ticket_id'=>$request->input('ticket_id'),
                'lvltwo_id'=>$request->input('lvltwo_id'),
                'lvlthr_id'=>$request->input('lvlthr_id'),
                'user_id'=>$userid,
                'status'=>$request->input('statusselect'),
                'comment'=>$request->input('comment'),
                'returnStatus'=>$Return,
                'date_time'=>Carbon::now(),
                'uploadPath'=>$file_name
                ]);

            if($Return=="Return")
            {

                $lvlthr_id = $request->input('lvlthr_id');
                $getlvlthr_sup = DB::select("SELECT
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.level_three like '%$lvlthr_id%'");
                 // dd ($getlvlthr_sup);
                foreach($getlvlthr_sup as $value)
                {
                    $get_email = DB::select("SELECT
                                                    user_master.id,
                                                    user_master.email
                                                    FROM
                                                    user_master
                                                    WHERE
                                                    user_master.id = '$value->allo_user'");
                     // dd ($get_email);
                    $data=array();
                    $data[0] =$request->ticket_num;
                    $data[1] =$get_email[0]->email;
                    // dd ($data[1]);
                    if(!empty($get_email)){
                        try
                        {
                          
                            $sendEmails=Mail::send([], [], function ($message) use ($data) {
                                        
                                $user_name=DB::table('user_master')
                                            ->where('id',session('userid'))
                                            ->first(['username']);            
                                $email_address = $data[1];
                                
                                $get_content = DB::select("SELECT
                                                            tbl_prefix_notification.*
                                                            FROM
                                                            tbl_prefix_notification
                                                            WHERE
                                                            tbl_prefix_notification.name = 'return'");    
        
                                $content = $get_content[0]->content;
                                $MailBody="";
                                $MailBody .= $content;
                                $MailBody .= '<p>Return User - '.$user_name->username.'</p>';
                                $MailBody .= '<p>Ticket Id - '.$data[0].'</p>';
                                $MailBody .= $get_content[0]->greet_content;
                
                                $message->setBody($MailBody, 'text/html');
                                $message->from($get_content[0]->from);
                                $message->subject($get_content[0]->subject);
                                //$message->to('nadeeshad@fairfirst.lk', 'Nadeesha');
                                //$message->to($getEmailBody[0]->claimFormSendAddress);
                                //$message->cc('badra@iphonik.com', 'Nadeesha');
                                $message->to("$email_address");
                                    
                            }); 
                        }
                        catch (\Exception $e) 
                        {
                            $ipaddress = (new UsersController())->get_client_ip();
                            $username=session()->get('username');
                            Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email Send Failed !!!");
                                // return "false2";
                        }
                    }
                }
                DB::table('tbl_ticket_master')
                        ->where('id',$request->ticket_id)
                        ->update(['returnstatus'=>"1"]);

               
            }
            DB::table('tbl_ticket_master')
                ->where('id',$request->ticket_id)
                ->update(['status'=>$request->input('statusselect')]);

                return redirect()->back();
            
    }

    public function addcustomer_inq(\Illuminate\Http\Request $request){
        
        $pre_fix = "Callcenter Agent ";
        //echo"$pre_fix";
        //exit;
        DB::table('tbl_ticket_comment')->insert(
                ['ticket_id'=>$request->input('ticket_id'),
                'lvltwo_id'=>$request->input('lvltwo_id'),
                'lvlthr_id'=>$request->input('lvlthr_id'),
                'user_id'=>session('userid'),
                'status'=>$request->input('status'),
                'comment'=>$pre_fix."-".$request->input('inq_comment'),
                'returnStatus'=>"",
                'date_time'=>Carbon::now(),
                ]);
                

            return redirect()->back();
    }


    public function retrieveTicketsOfUser($userid,$status){
        $condition=array();

        $condition['user_id'] = $userid;
        if($status!='All'){
            $condition['tbl_ticket_master.status'] = $status;
        }

        return DB::table('tbl_ticket_master')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->join('csp_ticket_category_master','tbl_ticket_master.lvlone_id','=','csp_ticket_category_master.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->where($condition)
            ->get([
                'tbl_ticket_master.*',
                'csp_contact_master.title',
                'csp_contact_master.firstname',
                'csp_contact_master.lastname',
                'csp_ticket_category_master.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name'
            ]);
    }

    public function countTicketsOfUserOfDepartment($userid, $departmentid, $status){
        $condition = ['user_id'=>$userid, 'lvltwo_id'=>$departmentid];
        if($status!='All'){
            $condition['status']=$status;
        }

        return DB::table('tbl_ticket_master')
            ->where($condition)
            ->count('id');
    }

    public function retrieveTicketById($id){
        return DB::table('tbl_ticket_master')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->join('user_master','tbl_ticket_master.user_id','=','user_master.id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->where('tbl_ticket_master.id', $id)
            ->first([
                'tbl_ticket_master.*',
                'csp_contact_master.title',
                'csp_contact_master.firstname',
                'csp_contact_master.lastname',
                'csp_contact_master.email',
                'user_master.username',
                'csp_contact_master.contact_number',
                'csp_contact_master.primary_contact',
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name',
                'tbl_levelone.id AS lvloneid',
                'tbl_leveltwo.id AS lvltwoid',
                'tbl_levelthr.id AS lvlthrid'
            ]);
    }

    public function retrieveTicketComments($id){
        return DB::select('SELECT
                        tbl_ticket_comment.*,
                        `user_master`.`username`,
                        `tbl_leveltwo`.`name` AS `lvltwo_name`,
                        `tbl_levelthr`.`name` AS `lvlthr_name`,
                        tbl_ticket_comment.ticket_id,
                        tbl_ticket_master.tickettype,
                        tbl_ticket_master.ticketcapturemode,
                        tbl_levelone.`name` AS lvlone_name
                        FROM
                        tbl_ticket_comment
                        INNER JOIN user_master ON tbl_ticket_comment.user_id = user_master.id
                        INNER JOIN tbl_leveltwo ON tbl_ticket_comment.lvltwo_id = tbl_leveltwo.id
                        INNER JOIN tbl_levelthr ON tbl_levelthr.lvltwo_id = tbl_leveltwo.id AND tbl_ticket_comment.lvlthr_id = tbl_levelthr.id
                        INNER JOIN tbl_ticket_master ON tbl_ticket_master.id =  tbl_ticket_comment.ticket_id
                        INNER JOIN tbl_levelone ON tbl_ticket_master.lvlone_id = tbl_levelone.id
                        WHERE
                        tbl_ticket_comment.ticket_id = '.$id.' ORDER BY tbl_ticket_comment.date_time DESC');
    }
    public function retrieveTicketsbyid($ticket_id){
       
            
                 return DB::table('tbl_ticket_master')
                     ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                     ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                     ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
                     ->where('tbl_ticket_master.id', $ticket_id)
                     ->get([
                         'tbl_ticket_master.*',
                         'tbl_levelone.name AS lvlone_name',
                         'tbl_leveltwo.name AS lvltwo_name',
                         'tbl_levelthr.name AS lvlthr_name'
                     ]);
          
         }
    public function retrieveTicketsOfContact($contactid,$status){
       // echo $status;
            if($status==null) {
                return DB::table('tbl_ticket_master')
                    ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                    ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                    ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
                    ->where('tbl_ticket_master.id', $contactid)
                    ->get([
                        'tbl_ticket_master.*',
                        'tbl_levelone.name AS lvlone_name',
                        'tbl_leveltwo.name AS lvltwo_name',
                        'tbl_levelthr.name AS lvlthr_name'
                    ]);
            }else{
            //echo '1';
            return DB::table('tbl_ticket_master')
                ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
                ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
                ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
				->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
                ->where('csp_contact_master.id', $contactid)
                ->where('tbl_ticket_master.status', "$status")
                ->get([
                    'tbl_ticket_master.*',
                    'tbl_levelone.name AS lvlone_name',
                    'tbl_leveltwo.name AS lvltwo_name',
                    'tbl_levelthr.name AS lvlthr_name'
                ]);        }
        }

    public function retrieveReturnTicket(){
       $selectMaxDate = DB::select("SELECT * FROM tbl_ticket_master INNER JOIN tbl_ticket_comment ON tbl_ticket_comment.ticket_id = tbl_ticket_master.id ORDER BY tbl_ticket_comment.date_time DESC LIMIT 1");
	$ids= DB::select("SELECT tbl_ticket_comment.ticket_id FROM tbl_ticket_comment WHERE tbl_ticket_comment.returnStatus='Return' AND tbl_ticket_comment.date_time='".$selectMaxDate[0]->date_time."'");

	if(!empty($ids)){
        $array = json_decode(json_encode($ids),true);

        return DB::table('tbl_ticket_master')
            ->select(
                'tbl_ticket_master.*',
                DB::raw("CONCAT(csp_contact_master.title,'.',csp_contact_master.firstname,' ',csp_contact_master.lastname) AS contact_name"),
                DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->where('tbl_ticket_master.id','in' ,$array)
            ->get();
	}
    }



    public function viewTTDashboard(){

        if(Util::isAuthorized("ticketTransferDashboard")=='LOGGEDOUT'){
            return redirect('/');
}
        if(Util::isAuthorized("ticketTransferDashboard")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Tickets Transfer Dashboard","View");
        
        $lvloneid_list = "";
        $lvltwoid_list = "";
        $lvlthrid_list = "";
        $getlvlone=array();
        $getlvltwo=array();
        $getlvlthr=array();
        $getlvloneids =DB::select("SELECT   tbl_user_allocation.level_one,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));
        
        if(!empty($getlvloneids[0]->level_one))
        {
            $lvloneid_list = $getlvloneids[0]->level_one;
            $getlvlone =DB::select("SELECT   tbl_levelone.id,
                                            tbl_levelone.name,
                                            tbl_levelone.status
                                            FROM
                                            tbl_levelone
                                            WHERE
                                            tbl_levelone.id iN ($lvloneid_list)");

        }

        $getlvltwoids =DB::select("SELECT   tbl_user_allocation.level_two,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));

        if(!empty($getlvltwoids[0]->level_two))
        {
            $lvltwoid_list = $getlvltwoids[0]->level_two;
            $getlvltwo =DB::select("SELECT   tbl_leveltwo.id,
                                                tbl_leveltwo.name,
                                                tbl_leveltwo.status
                                                FROM
                                                tbl_leveltwo
                                                WHERE
                                                tbl_leveltwo.id iN ($lvltwoid_list)");  

        }

        $getlvlthrids =DB::select("SELECT   tbl_user_allocation.level_three,
                                                    tbl_user_allocation.allo_user
                                                    FROM
                                                    tbl_user_allocation
                                                    WHERE
                                                    tbl_user_allocation.allo_user=".session('userid'));  
         if(!empty($getlvlthrids[0]->level_three))
         {
            $lvlthrid_list = $getlvlthrids[0]->level_three;
            $getlvlthr =DB::select("SELECT   tbl_levelthr.id,
                                            tbl_levelthr.name,
                                            tbl_levelthr.status
                                            FROM
                                            tbl_levelthr
                                            WHERE
                                            tbl_levelthr.id iN ($lvlthrid_list)");  
         }

 		$ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User open the Reallocation dashboard-$username",$username,"View Reallocation");

        return view('TicketTransferDashboard')
            ->with('lvlone',$getlvlone)
            ->with('lvltwo',$getlvltwo)
            ->with('lvlthr',$getlvlthr);
            //->with('subdepartments', (new DepartmentController())->retrieveSubDepartments());
            //->with('checkStatus',$this->checkTicketWiseStatus());
    }
    
    public function toBeCompleteDashBoard(){
	 if(Util::isAuthorized("")=='LOGGEDOUT'){
            return redirect('/');
}
        if(Util::isAuthorized("toBeCompleteDashBoard")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Tickets Transfer Dashboard","View");

        $lvloneid_list = "";
        $lvltwoid_list = "";
        $lvlthrid_list = "";
        $getlvlone=array();
        $getlvltwo=array();
        $getlvlthr=array();
        $getlvloneids =DB::select("SELECT   tbl_user_allocation.level_one,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));
        
        if(!empty($getlvloneids[0]->level_one))
        {
            $lvloneid_list = $getlvloneids[0]->level_one;
            $getlvlone =DB::select("SELECT   tbl_levelone.id,
                                            tbl_levelone.name,
                                            tbl_levelone.status
                                            FROM
                                            tbl_levelone
                                            WHERE
                                            tbl_levelone.id iN ($lvloneid_list)");

        }

        $getlvltwoids =DB::select("SELECT   tbl_user_allocation.level_two,
                                            tbl_user_allocation.allo_user
                                            FROM
                                            tbl_user_allocation
                                            WHERE
                                            tbl_user_allocation.allo_user=".session('userid'));

        if(!empty($getlvltwoids[0]->level_two))
        {
            $lvltwoid_list = $getlvltwoids[0]->level_two;
            $getlvltwo =DB::select("SELECT   tbl_leveltwo.id,
                                                tbl_leveltwo.name,
                                                tbl_leveltwo.status
                                                FROM
                                                tbl_leveltwo
                                                WHERE
                                                tbl_leveltwo.id iN ($lvltwoid_list)");  

        }

        $getlvlthrids =DB::select("SELECT   tbl_user_allocation.level_three,
                                                    tbl_user_allocation.allo_user
                                                    FROM
                                                    tbl_user_allocation
                                                    WHERE
                                                    tbl_user_allocation.allo_user=".session('userid'));  
         if(!empty($getlvlthrids[0]->level_three))
         {
            $lvlthrid_list = $getlvlthrids[0]->level_three;
            $getlvlthr =DB::select("SELECT   tbl_levelthr.id,
                                            tbl_levelthr.name,
                                            tbl_levelthr.status
                                            FROM
                                            tbl_levelthr
                                            WHERE
                                            tbl_levelthr.id iN ($lvlthrid_list)");  
         }
	 	 $ipaddress = (new UsersController())->get_client_ip();
         $username=session()->get('username');
         Util::user_auth_log($ipaddress,"User open the Followed-Ups dashboard-$username",$username,"View Followed-Ups"); 


	 return view('toBeCompleteDashboard')
                ->with('lvlone',$getlvlone)
                ->with('lvltwo',$getlvltwo)
                ->with('lvlthr',$getlvlthr);
    }
     public function retrieveCompleteTicket(){

        return DB::table('tbl_ticket_master')
            ->select(
                'tbl_ticket_master.*',
                DB::raw("CONCAT(csp_contact_master.title,'.',csp_contact_master.firstname,' ',csp_contact_master.lastname) AS contact_name"),
                DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
            ->get();
    }
    
     public function searchcompletetickets(){
	    $data=array();
        $condition1 = array();
        $condition2 =array();
        $condition3 =array();
        $condition4 =array();

  
        $status= Request::query('status');
        if(Request::query('status')!='All'){
            $condition['tbl_ticket_master.status'] = $status;
        }else{
            $condition = array();
        }

    
        $startdate= Request::query('startdate_val');
        $enddate= Request::query('enddate_val');
        $user=session('userid');
            


        $ids = DB::select("SELECT tbl_ticket_master.id FROM tbl_ticket_master WHERE tbl_ticket_master.status in('Cancelled','Closed')");

	if(!empty($ids)){
        $array = json_decode(json_encode($ids),true);
        //print_r($array);
            $dataq = DB::table('tbl_ticket_master')
            ->select(
                'tbl_ticket_master.*',
                'user_master.username',
                'tbl_ticket_master.date_time AS createddate',
                DB::raw("CONCAT(csp_contact_master.title,'.',csp_contact_master.firstname,' ',csp_contact_master.lastname) AS contact_name"),
                DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('user_master','tbl_ticket_master.tktUser','=','user_master.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id');
            
            if($status=='All'){
                $data = $dataq->wherein('tbl_ticket_master.status',['Closed','Cancelled']);
            }
            $dataq = $dataq ->where(function($query) {
                $user=session('userid');
                $allocated_lvlone =  DB::table('tbl_user_allocation')
                            ->where('allo_user', $user)
                            ->select('level_one','level_two','level_three')->get();

                if(!$allocated_lvlone->isEmpty())
                {
                    $all_lvlones = explode(",",$allocated_lvlone[0]->level_one);
                    $all_lvltwos = explode(",",$allocated_lvlone[0]->level_two);
                    $all_lvlthrs = explode(",",$allocated_lvlone[0]->level_three);
                }else
                {   
                    $all_lvlones=array();
                    $all_lvltwos=array();
                    $all_lvlthrs=array();
                }
                
                $query->wherein('tbl_ticket_master.lvlone_id',$all_lvlones)
                        ->orWherein('tbl_ticket_master.lvltwo_id', $all_lvltwos)
                        ->orWherein('tbl_ticket_master.lvlthr_id', $all_lvlthrs);
                //$query->where('csp_ticket_master.user_id', '=', session('userid'))
               
                    });
            /*$data = $dataq->where(function ($query) {
                $user=session('userid');
                $lvlone_id = Request::query('lvlone_id');

                if(Request::query('lvlone_id')!="All")
                {
                        $condition3['tbl_ticket_master.lvlone_id'] = $lvlone_id;
                }
                $department= Request::query('lvltwo_id');
                if(Request::query('lvltwo_id')!='All'){
                    $condition1['tbl_ticket_master.lvltwo_id'] = $department;
                }else{
                    $condition1 = array();
                }

                $lvlthr_id= Request::query('lvlthr_id');
                if(Request::query('lvlthr_id')!='All'){
                    $condition2['tbl_ticket_master.lvlthr_id'] = $lvlthr_id;
                }else{
                    $condition2 = array();
                }
                $allocated_lvlone =  DB::table('tbl_user_allocation')
                                        ->where('allo_user', $user)
                                        ->select('level_one')->get();
                $all_lvlones = explode(",",$allocated_lvlone[0]->level_one);
                if(!empty($condition3)){
                    $query ->where($condition3);
                }else
                {
                   if($allocated_lvlone[0]->level_one!=null)
                    {
                        $query->orWhereIn('tbl_ticket_master.lvlone_id',$all_lvlones);
                    }
                }

                $allocated_lvltwo =  DB::table('tbl_user_allocation')
                                        ->where('allo_user', $user)
                                        ->select('level_two')->get();
                $all_lvltwos = explode(",",$allocated_lvltwo[0]->level_two);
                if(!empty($condition1)){
                    $query ->where($condition1);
                }else
                {
                    if($allocated_lvltwo[0]->level_two!=null)
                    {
                        $query ->orWhereIn('tbl_ticket_master.lvltwo_id',$all_lvltwos);
                    }
                }
                $allocated_lvlthr =  DB::table('tbl_user_allocation')
                ->where('allo_user', $user)
                ->select('level_three')->get();
                
                $all_lvlthrs = explode(",",$allocated_lvlthr[0]->level_three);
                if(!empty($condition2)){
                    $query ->where($condition2);
                }else
                {
                    if($allocated_lvlthr[0]->level_three!=null)
                    {
                        $query ->WhereIn('tbl_ticket_master.lvlthr_id',$all_lvlthrs);
                    }
                }

                
            });*/
         
            if(!empty($condition))
            {
                $data = $dataq ->where($condition);
            }
            
            
            if($status!='All'){
                $data = $dataq ->where($condition4);
            }
            $data = $dataq ->whereBetween('tbl_ticket_master.date_time', array($startdate, $enddate));
            $data = $dataq->groupby('tbl_ticket_master.id')
            ->get();
	    }
			$ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"User Search Followed-Ups",$username,"Search Followed-Ups");
            return compact('data',$data);
}
    
    public function viewCompleteticket($id){
	 if(Util::isAuthorized("viewCompleteticket")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("viewCompleteticket")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log("View Ticket Details","View");

        $ticket=$this->retrieveTicketById($id);
        $comments=$this->retrieveTicketComments($id);
        return view('CompleteTicket')
            ->with('ticket',$ticket)
            ->with('comments',$comments)
            ->with('updatepermission',Util::isAuthorized("UpdateTicketStatus"))
            ->with('checkStatus',$this->checkTicketCompleteStatus($id));
    }
    
    public function completeJob(\Illuminate\Http\Request $request){

       Util::log("View Ticket Details","Update Status");

        $status = $request->input('status');

        if ($status == 'reopen') {

            $pre = "Ticket Reopened by ".session('username');           
            // code...
            $statusTick = 'Work In Progress';
            $reopen = 'Reopened';
            // $completeDate = NULL;

            DB::table('tbl_ticket_comment')->insert([
                'ticket_id'=>$request->ticket_id,
                'lvltwo_id'=>$request->department_id,
                'lvlthr_id'=>$request->subdepartment_id,
                'user_id'=>session('userid'),
                'status'=>$reopen,
                'comment'=>$pre."-".$request->input('comment'),
                'returnStatus'=>"",
                'date_time'=>Carbon::now(),
            ]);

        }else{
            $statusTick = 'Complete';
            $reopen = NULL;
            // $completeDate = Carbon::now();
             $preCom = "Ticket Completed by ".session('username');

             DB::table('tbl_ticket_comment')->insert([
                'ticket_id'=>$request->ticket_id,
                'lvltwo_id'=>$request->department_id,
                'lvlthr_id'=>$request->subdepartment_id,
                'user_id'=>session('userid'),
                'status'=> 'Complete',
                'comment'=>$preCom."-".$request->input('comment'),
                'returnStatus'=>"",
                'date_time'=>Carbon::now(),
            ]);
        }

        DB::table('tbl_ticket_master')
        ->where('id',$request->ticket_id)
        ->update(['status'=>$statusTick,'complete_remark'=>$request->input('comment'),'completeUser'=>session('userid'),'complete_date'=>Carbon::now(), 'reopen' => $reopen]);
    }
    
    public function checkTicketCompleteStatus($id){
	
	return DB::select("SELECT status FROM tbl_ticket_master WHERE tbl_ticket_master.id=$id");
    }
     public function retrieveTicketsDepartmentWise($userid,$status){
	 $condition=array();
    
        $condition['csp_department_user.user_id'] = $userid;
        if($status!='All'){
            $condition['tbl_ticket_master.status'] = $status;
        }
   
        return DB::table('tbl_ticket_master')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id')
		->join('csp_department_user','csp_department_user.lvltwo_id','=','tbl_leveltwo.id')
            ->where($condition)
            ->get([
                'tbl_ticket_master.*',
                'csp_contact_master.title',
                'csp_contact_master.firstname',
                'csp_contact_master.lastname',
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name'
            ]);
}
 public function searchtickets(){  
	    $condition = array();
        $condition1 = array();
        $condition2 = array();
        $condition3 = array();
        $condition4 = array();
    
        $lvlone_id = Request::query('lvlone_id');

        if(Request::query('lvlone_id')!="All")
        {
                $condition3['tbl_ticket_master.lvlone_id'] = $lvlone_id;
        }


        $status= Request::query('status');
        if(Request::query('status')!='All'){
            $condition['tbl_ticket_master.status'] = $status;
        }else{
            $condition = array();
        }

        $department= Request::query('lvltwo_id');
        if(Request::query('lvltwo_id')!='All'){
            $condition1['tbl_ticket_master.lvltwo_id'] = $department;
        }else{
            $condition1 = array();
        }

        $subdepartment= Request::query('lvlthr_id');
        if(Request::query('lvlthr_id')!='All'){
            $condition2['tbl_ticket_master.lvlthr_id'] = $subdepartment;
        }else{
            $condition2 = array();
        }

        $startdate= Request::query('startdate_val');
        $enddate= Request::query('enddate_val');
       
        $user=session('userid');
        //$ccount = DB::select("SELECT * From csp_department_user where user_id=$user");
        $subdept= DB::select("SELECT tbl_levelthr.id from tbl_levelthr");

            $dataq = DB::table('tbl_ticket_master')
            ->select(
                'tbl_kpilevel_info.*',
                'tbl_ticket_master.*',
                'tbl_ticket_master.date_time AS createddate',
                DB::raw('(select SUM(TIMESTAMPDIFF(hour, on_hold_start, on_hold_end)) 
                AS tot_on_hold from tbl_onhold_operation where tbl_ticket_master.id  =   tbl_onhold_operation.ticket_id 
                group by ticket_id) as tot_on_hold'),
                'tbl_ticket_master.id AS tot_hours',
                DB::raw("CONCAT(csp_contact_master.title,'.',csp_contact_master.firstname, ' ', csp_contact_master.lastname) AS contact_name"),
                DB::raw("MAX(tbl_ticket_comment.date_time) AS latest_comment_date_time"),
                'tbl_levelone.name AS lvlone_name',
                'tbl_leveltwo.name AS lvltwo_name',
                'tbl_levelthr.name AS lvlthr_name')
            ->join('csp_contact_master','tbl_ticket_master.contact_id','=','csp_contact_master.id')
            ->leftJoin('tbl_ticket_comment','tbl_ticket_master.id','=','tbl_ticket_comment.ticket_id')
            ->join('tbl_levelone','tbl_ticket_master.lvlone_id','=','tbl_levelone.id')
            ->join('tbl_kpilevel_info','tbl_ticket_master.kpilevel_id','=','tbl_kpilevel_info.id')
            ->join('tbl_leveltwo','tbl_ticket_master.lvltwo_id','=','tbl_leveltwo.id')
            ->join('tbl_levelthr','tbl_ticket_master.lvlthr_id','=','tbl_levelthr.id');


            if(!empty($condition)){
             $dataq = $dataq->where($condition);
            }
            if(!empty($condition1)){
                $dataq = $dataq->where($condition1);
            }
            if(!empty($condition2)){
                $dataq = $dataq->where($condition2);
            }
            if(!empty($condition3)){
                $dataq = $dataq->where($condition3);
                }
                    $dataq ->whereBetween('tbl_ticket_master.date_time', array($startdate, $enddate));
                    $dataq ->where('tbl_ticket_master.contact_id', '=',Request::query('contact_id'));

                    $data = $dataq->groupby('tbl_ticket_master.id')
                    ->get();
                    foreach($data as $value)
                    {
                    
                       $tot_hours= $this->kpiholidaycal($value->date_time);
                       $data_arr = json_decode(json_encode($data), true); 
                       $key = array_search($value->ticket_num, array_column($data_arr, 'ticket_num'));
                       $data[$key]->tot_hours = $tot_hours;
                    
                    }
                return compact('data',$data);

        
	    
    }
	public function add_remainder(\Illuminate\Http\Request $request){
        
        $ticket_id = $request->input('ticket_id');
        $rem_datetime = $request->input('rem_datetime');
        $rem_des = $request->input('rem_des');
        $user_id=session('userid');
     
					DB::table('tbl_remind_mst')->insert(
							['ticket_id'=>$ticket_id,
							'rem_datetime'=>$rem_datetime,
							'rem_des'=>$rem_des,
							'cre_userid'=>$user_id,
							'status'=>"Fresh",
							'cre_datetime'=>Carbon::now(),
							]);
		
                return redirect()->back();
            
    }
	public function updateholdStatus(\Illuminate\Http\Request $request){
        //echo $request->input('returnStatus');
        //exit();
     
        Util::log("View Ticket Details","Update Status");
        $selectExistRec= DB::select("SELECT * from tbl_onhold_operation WHERE ticket_id='".$request->ticket_id."'  ORDER BY id DESC LIMIT 1");
		if(!empty($selectExistRec)){
		if($selectExistRec[0]->on_hold_end==""){
			
			  DB::table('tbl_onhold_operation')
                ->where('id',$selectExistRec[0]->id)
                ->update(['on_hold_end'=>Carbon::now(),'end_user'=>session('userid'),'end_description'=>$request->input('comment')]);
				$pre_fix = "Ticket Unlock";
				   // echo"$pre_fix";
					//exit;
					DB::table('tbl_ticket_comment')->insert(
							['ticket_id'=>$request->input('ticket_id'),
							'lvltwo_id'=>$request->input('lvltwo_id'),
							'lvlthr_id'=>$request->input('lvlthr_id'),
							'user_id'=>session('userid'),
							'status'=>$request->input('statusselect'),
							'comment'=>$pre_fix."-".$request->input('comment'),
							'returnStatus'=>"",
							'date_time'=>Carbon::now(),
							]);
		}else{
			
			DB::table('tbl_onhold_operation')->insert(
                ['ticket_id'=>$request->input('ticket_id'),
                'start_description'=>$request->input('comment'),
                'on_hold_start'=>Carbon::now(),
                'start_user'=>session('userid')
                ]);
				$pre_fix = "Ticket Lock";
				   // echo"$pre_fix";
					//exit;
					DB::table('tbl_ticket_comment')->insert(
							['ticket_id'=>$request->input('ticket_id'),
							'lvltwo_id'=>$request->input('lvltwo_id'),
							'lvlthr_id'=>$request->input('lvlthr_id'),
							'user_id'=>session('userid'),
							'status'=>$request->input('statusselect'),
							'comment'=>$pre_fix."-".$request->input('comment'),
							'returnStatus'=>"",
							'date_time'=>Carbon::now(),
							]);
		
		}
		}else{
			DB::table('tbl_onhold_operation')->insert(
                ['ticket_id'=>$request->input('ticket_id'),
                'start_description'=>$request->input('comment'),
                'on_hold_start'=>Carbon::now(),
                'start_user'=>session('userid')
                ]);
				$pre_fix = "Ticket Lock";
				   // echo"$pre_fix";
					//exit;
					DB::table('tbl_ticket_comment')->insert(
							['ticket_id'=>$request->input('ticket_id'),
							'lvltwo_id'=>$request->input('lvltwo_id'),
							'lvlthr_id'=>$request->input('lvlthr_id'),
							'user_id'=>session('userid'),
							'status'=>$request->input('statusselect'),
							'comment'=>$pre_fix."-".$request->input('comment'),
							'returnStatus'=>"",
							'date_time'=>Carbon::now(),
							]);
		}
            

        
            DB::table('tbl_ticket_master')
                ->where('id',$request->ticket_id)
                ->update(['status'=>$request->input('statusselect')]);

                return redirect()->back();
            
    }
    public function esc_ticketdetail_sendbyemail()
    {
        $ticket_id =  request::input('ticket_id');
        $get_lvl_one = $_GET['lvl_one'];
        
        $get_lvl_two = $_GET['lvl_two'];

        $get_lvl_thr = $_GET['lvl_thr'];

        $user_id=session('userid');
        
        $get_emails = [];
        $ticket=$this->retrieveTicketById($ticket_id);

        $ticket_num = $ticket->ticket_num;

        $get_cif_num = DB::select("SELECT tbl_amana_cif_info.Customer_CIF,csp_contact_master.primary_contact,csp_contact_master.email 
                        FROM tbl_amana_cif_info 
                        INNER JOIN csp_contact_master ON tbl_amana_cif_info.Primary_NIC = csp_contact_master.nic 
                        INNER JOIN tbl_ticket_master ON csp_contact_master.id = tbl_ticket_master.contact_id 
                        WHERE tbl_ticket_master.ticket_num = '".$ticket_num."' LIMIT 1  ");

        foreach($get_cif_num as $values)
            {
                if (isset($values->Customer_CIF)) 
                {
                    $cif = $values->Customer_CIF;
                }
                else
                {
                     $cif = "";
                }
               if (isset($values->primary_contact)) 
                {
                    $get_cif_mob_num = $values->primary_contact;
                }
                else
                {
                     $get_cif_mob_num = "";
                }
               if (isset($values->email)) 
                {
                    $get_cus_email = $values->email;
                }
                else
                {
                     $get_cus_email = "";
                }

            }
        $get_sup_allocation = DB::select("SELECT
                        user_master.email
                        FROM
                        tbl_user_allocation
                        INNER JOIN user_master ON tbl_user_allocation.allo_user = user_master.id 
                        WHERE
                        tbl_user_allocation.level_one LIKE '%$get_lvl_one%' and tbl_user_allocation.level_two LIKE '%$get_lvl_two%' 
                        and tbl_user_allocation.level_three LIKE '%$get_lvl_thr%' ");
     //print_r($get_sup_allocation);  exit();

        foreach($get_sup_allocation as $values)
            {
                if($values->email!='')
                {
                    array_push($get_emails,$values->email);
                }
            }
             // print_r($get_emails);  exit();
        $get_assignuser = DB::select("SELECT
                        user_master.email
                        FROM
                        kpi_notify_user
                        INNER JOIN user_master ON kpi_notify_user.user_id = user_master.id 
                        WHERE
                        kpi_notify_user.kpilevel_id = '$get_lvl_thr' Limit 1 ");

        foreach($get_assignuser as $values)
            {
                //  print_r($values);  exit();
                if($values->email!='')
                {
                    array_push($get_emails,$values->email);
                }
                
            }

         $get_allocated_agents = DB::select("SELECT
                                user_master.email
                                FROM
                                tbl_agent_allocation
                                INNER JOIN user_master ON tbl_agent_allocation.allo_agent_id = user_master.id 
                                WHERE tbl_agent_allocation.lvlthr_id = '$get_lvl_thr'  ");
        // dd($get_allocated_agents);  
          foreach($get_allocated_agents as $values)
            {
                if($values->email!='')
                {
                    array_push($get_emails,$values->email);
                }
                
            }
            $tkt_usr = $ticket->tktUser;

            $tkt_usr = DB::select("SELECT
                                    user_master.email,user_master.fname
                                    FROM
                                    tbl_ticket_master
                                    INNER JOIN user_master ON tbl_ticket_master.tktUser = user_master.id 
                                    WHERE tbl_ticket_master.tktUser = '$tkt_usr' Limit 1
                                     ");

            foreach($tkt_usr as $values)
            {
                if($values->email!='')
                {
                    array_push($get_emails,$values->email);
                }
                if (isset($values->fname)) 
                {
                    $get_sup_fname = $values->fname;
                }
                else
                {
                     $get_sup_fname = "";
                }
            }
//print_r($get_cif); exit();
        $data[0] =$ticket->ticket_num;
        $data[1] =$get_emails; 
       // $data[2] ="test";
        if (isset($cif)) 
        {
            $data[2] = $cif;
        }
        else
        {
            $data[2]= "";
        }
        if (isset($get_cif_mob_num)) 
        {
            $data[3] = $get_cif_mob_num;
        }
        else
        {
            $data[3]= "";
        }
        if (isset($get_cus_email)) 
        {
            $data[4] = $get_cus_email;
        }
        else
        {
            $data[4]= "";
        }
        if (isset($get_sup_fname)) 
        {
            $data[5] = $get_sup_fname;
        }
        else
        {
            $data[5]= "";
        }
        if($data[1]=="")
            {
                return "false";
            }
        else
            {
                try{    
                    $sendEmails=Mail::send([],[], function ($message) use ($data) 
                            {    
                                $email_address = $data[1];
                                $MailBody='<p>The Ticket id (Ref-'.$data[0].') has not been closed by '.$data[5].' </p>';
                                $MailBody .= '<p>Ticket KPI Level Expiration Notification</p>';
                                $MailBody .= '<p>CIF No : '.$data[2].'  </p>';
                                $MailBody .= '<p>Mobile Number : '.$data[3].'</p>';
                                $MailBody .= '<p>Email Address : '.$data[4].'</p>';
                                $MailBody .= '<p><b>Note: This is system generated email.Please refrain from replying.</b></p>';
                                $MailBody .= '<p>Your thoughts on the matter are highly appreciated.</p>';
                                 $MailBody .= '<p>Thank you and Best Regards.</p>';
                                
                                $message->setBody($MailBody, 'text/html');
                                $message->from('ccsystememail@amana.lk');
                                $message->subject('Ticket KPI Level Expiration Notification');
                                foreach($email_address as $values)
                                    {
                                        $message->to($values);
                                    }
                            }); 
                             DB::table('tbl_ticket_master')
                            ->where('id',$ticket_id)
                            ->update(['escalated_usr'=>session('userid'),'esc_datetime'=>Carbon::now(),'status_of_esc'=>"Yes"]);            
                    } 
                catch (\Exception $e)
                {
                    $ipaddress = (new UsersController())->get_client_ip();
                    $username=session()->get('username');
                    Util::user_auth_log($ipaddress,$e->getMessage(),$username,"Email send Failed ");

                     DB::table('tbl_ticket_master')
                            ->where('id',$ticket_id)
                            ->update(['escalated_usr'=>session('userid'),'esc_datetime'=>Carbon::now(),'status_of_esc'=>"Yes"]);
                    return "false2";

                }
                 return "true";
             }

        }
   
   
}