<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;


require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class BreakApprovalController extends Controller
{
  
    
    public function view_br_approval_master(){	
		
		if(Util::isAuthorized("view_br_approval_master")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_br_approval_master")=='DENIED'){
            return view('permissiondenide');
        }
		
        Util::log('Break Approval Master File','View');
        
        $userid=session('userid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();


        $get_br_appr_det = DB::table('tbl_break_schedule_master')
                                    ->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Break Approval Master File",$username,"View Break Approval Master File");


        return view('view_br_approval_master',compact('get_br_appr_det'));

	} 
    public function view_add_br_approval()
    {   
        if(Util::isAuthorized("view_add_br_approval")=='LOGGEDOUT')
        {
            return redirect('/');
        }
        if(Util::isAuthorized("view_add_br_approval")=='DENIED')
        {
            return view('permissiondenide');
        }

        Util::log('View Add Break Approval','View');
        
        $userid=session('userid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
      
        $get_com_data  = DB::table('tbl_com_mst')->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Add Break Approval Dashboard",$username,"View Add Break Approval");

        return view('view_add_br_approval');
    }
    public function view_br_req_approval()
    {   
        if(Util::isAuthorized("view_br_req_approval")=='LOGGEDOUT')
        {
            return redirect('/');
        }
        if(Util::isAuthorized("view_br_req_approval")=='DENIED')
        {
            return view('permissiondenide');
        }

        Util::log('View Break Request Reminder Dashboard','View');
        
        $userid=session('userid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getbreak_reject_reasons = DB::table('tbl_br_req_pre_remark')
                                            ->select('remark','id')
                                            ->get();
        $get_id = DB::table('tbl_br_req_pre_remark')
                        ->select('id')
                        ->get();
      
        $get_com_data  = DB::table('tbl_com_mst')->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Break Request Reminder Dashboard",$username,"View Break Request Reminder Dashboard");

        return view('view_br_req_approval',compact('getbreak_reject_reasons','get_id'));
    }
    
    public function br_approval_req(Request $request) {
        $userid=session()->get('userid');
        $com_id = Util::get_com_id_by_user($userid);
        //  print_r($com_id);exit();
        $data  =    DB::table('tbl_break_request')
                            ->select('tbl_break_request.id','tbl_com_mst.com_name','tbl_break_request.sip_id','user_master.username','tbl_break_request.break_type')
                            ->join('user_master', 'tbl_break_request.user_id', '=', 'user_master.id') 
                            ->join('tbl_com_mst', 'user_master.com_id', '=', 'tbl_com_mst.id')
                            ->where('user_master.com_id',$com_id)
                            ->where('tbl_break_request.sup_action',"=","Fresh")
                            ->get();
        // print_r($data);exit();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Break Approval List",$username,"Search Break Approval List");
        
        return compact('data');
    } 
    public function br_reject_with_pen_time(Request $request)
    {
        $id = $_GET['id'];
        $br_reject_reason= $request->input('br_reject_penal_reason');
        $br_reject_pen_time= $request->input('br_reject_pen_time');
        $sup_action = $_GET['sup_action_1'];
        $userid=session('userid'); 
        
        DB::table('tbl_break_request')
            ->where('id', $id)
            ->update(['sup_action' => $sup_action,
                      'sup_userid'=>$userid,
                      'sup_remark'=>$br_reject_reason,
                      'sup_penalty_time'=>$br_reject_pen_time,
                      'sup_act_datetime'=>Carbon::now()]);
        return redirect()->back();
    }
    public function br_reject_reason()
    {
        $id = $_GET['rec_id'];
        // print_r($id);exit();
        $br_reject_reason = $_GET['br_reject_reason'];
        $sup_action = $_GET['sup_action'];
        $userid=session('userid'); 
        
         DB::table('tbl_break_request')
            ->where('id', $id)
            ->update(['sup_action' => $sup_action,
                      'sup_userid'=>$userid,
                      'sup_remark'=>$br_reject_reason,
                      'sup_act_datetime'=>Carbon::now()]);
        return redirect()->back();
    }
    
    public function approvebreakreq()
    {
        $id = $_GET['id'];
        $action = $_GET['action'];
        $userid=session('userid'); 
        
         DB::table('tbl_break_request')
            ->where('id', $id)
            ->update(['sup_action' => $action,
                      'sup_userid'=>$userid,
                      'sup_act_datetime'=>Carbon::now()
                      ]);
        return redirect()->back();
    }

    public function save_br_approval(Request $request) {
        $userid=session('userid');     
        // print_r($username);exit();
        $week_day = $request->input('week_day');  
        $frm_time = $request->input('frm_time'); 
        $end_time = $request->input('end_time');
        $userstatus = $request->input('userstatus');

        if($userstatus == '1'){
            $status = '1' ;
        }else
        {
            $status = '0' ;
        }
        $get_com_id  = DB::table('user_master')
                                ->where('id',$userid)
                                ->first();
        $data = array(
            'rec_date_time' =>Carbon::now(),
            'user_id' =>  $userid,
            'record_day' => $week_day,
            'start_time' => $frm_time,
            'end_time' =>  $end_time,
            'status' => $status,
            'com_id' =>$get_com_id->com_id
            );
            DB::table('tbl_break_schedule_master')
                    ->insert($data);
            
        return view('view_add_br_approval');
    } 

 public function search_br_approval_list()
    {  
        // $data  = DB::table('tbl_break_schedule_master')
        //                     ->select('rec_date_time','User','record_day','start_time','end_time','status')
        //                     ->get();
        $data=DB::SELECT("SELECT * FROM tbl_break_schedule_master");

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Break Approval List",$username,"Search Break Approval List");
        
        return compact('data');
        
    }
    public function status_update()
    {  
        $br_appr_id = $_GET['id'];
        // print($br_appr_id);exit();
        $status_br_appr  = DB::table('tbl_break_schedule_master') 
                        ->where('id', $br_appr_id)
                        ->first();
        $status_=0;
		if($status_br_appr->status == 0){
			$status_=1;
		}else if($status_br_appr->status == 1){
			$status_=0;
		}
        DB::table('tbl_break_schedule_master')
                ->where('id', $br_appr_id)
                ->update(['status' => $status_]);
    }
    public function br_appr_delete()
    {

         $id =  $_GET['id']; 
         DB::table('tbl_break_schedule_master')
                    ->where('id',$id)
                    ->delete();

        echo '1' ;        
    }
    public function view_edit_br_appr_shedule($shed_id){	
		// print_r($shed_id);exit();
		if(Util::isAuthorized("view_edit_br_appr_shedule")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_edit_br_appr_shedule")=='DENIED'){
            return view('permissiondenide');
        }
		
        Util::log('Edit Break Approval Master File','View');

        $userid=session()->get('userid');
		$com_id = Util::get_com_id_by_user($userid);

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $get_br_appr_det = DB::table('tbl_break_schedule_master')
                                    ->where('com_id',$com_id)
                                    ->where('id',$shed_id)
                                    ->get();
        // print_r($get_br_appr_det);exit();
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Break Approval Master File",$username,"View Break Approval Master File");

        return view('view_edit_br_appr_shedule',compact('get_br_appr_det'));

	}
    
    public function save_edit_br_approval(Request $request) {
        $userid=session('userid');
        $week_day = $request->input('week_day'); 
        $frm_time = $request->input('frm_time'); 
        $end_time = $request->input('end_time');
        $userstatus = $request->input('userstatus');
        $id = $request->input('id');
        //  print_r($id);exit();
        if($userstatus == '1'){
            $status = '1' ;
        }else
        {
            $status = '0' ;
        }
        $get_com_id  = DB::table('user_master')
                                ->where('id',$userid)
                                ->first();
        $data = array(
            'rec_date_time' =>Carbon::now(),
            'user_id' =>  $userid,
            'record_day' => $week_day,
            'start_time' => $frm_time,
            'end_time' =>  $end_time,
            'status' => $status,
            'com_id' =>$get_com_id->com_id
            );
        DB::table('tbl_break_schedule_master')
            ->where('id', $id)
            ->update($data); 
            
         return redirect('view_br_approval_master');
    } 
}