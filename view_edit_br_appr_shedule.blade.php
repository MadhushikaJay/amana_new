<!DOCTYPE html>
@include('header_new')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="form_caption"> Edit Break Approval</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <form class="form-horizontal" action="../save_edit_br_approval" method="post" id="form">
                {{csrf_field()}}

                    @if(count($errors))
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                        </div>
                    @endif
                    
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            <span class="glyphicon glyphicon-ok">
                            </span>
                            <em> {!! session('flash_message') !!}</em>
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @foreach($get_br_appr_det as $get_br_appr_det_)
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left:0px;">
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">Week Days</label>
                                    </div>
                                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                    <select class="form-control select2 custom-select-value" name="week_day" id="week_day" value="" style="width: 100%" required>
                                        <option <?php if($get_br_appr_det_->record_day=='Sunday'){ ?> selected="selected" <?php } ?> value="Sunday">Sunday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Monday'){ ?> selected="selected" <?php } ?> value="Monday">Monday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Tuesday'){ ?> selected="selected" <?php } ?> value="Tuesday">Tuesday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Wednesday'){ ?> selected="selected" <?php } ?> value="Wednesday">Wednesday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Thursday'){ ?> selected="selected" <?php } ?> value="Thursday">Thursday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Friday'){ ?> selected="selected" <?php } ?> value="Friday">Friday</option>
                                        <option <?php if($get_br_appr_det_->record_day=='Saturday'){ ?> selected="selected" <?php } ?> value="Saturday">Saturday</option>
                                    </select>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">Start Time</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 15px; padding-right: 0px;">
                                        <input type="time" class="some_class" 
                                        value="<?php echo date($get_br_appr_det_->start_time);  ?>" name="frm_time" id="frm_time">
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">End Time</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 15px; padding-right: 0px;">
                                        <input type="time" class="some_class" 
                                        value="<?php echo date($get_br_appr_det_->end_time); ?>" name="end_time" id="end_time">
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="userstatus" class="login2 pull-right pull-right-pro required">Status</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"  style="padding-top: 15px; padding-right: 0px;">
                                        <input type="radio"  name="userstatus" value="1" <?php if($get_br_appr_det_->status=='1'){ ?> checked="checked" <?php } ?>  required="" style="width: 10%;"> Active
                                        <input type="radio" name="userstatus" value="0" <?php if($get_br_appr_det_->status=='0'){ ?> checked="checked" <?php } ?> required="" style="width: 10%;"> Inactive
                                    </div>
                               </div>
                            </div>    
                            </div>
                            <input type="hidden" name="id" id="id" value="{{$get_br_appr_det_->id}}" >
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" margin-top:10px;">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2" style="margin-left: 61px;">
                                <button type="submit" class="btn btn-custon-four btn-success attr_btn" name="submit">Submit</button>
                                <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="cancel()">Cancel</button>
                        </div>
                    </div> 
                @endforeach
                </form>
			</div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    function cancel() {
        location.href = "{{url ('view_br_approval_master')}} ";
    }
</script>
<script>


$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
@include('footer')

</body>
</html>
      
