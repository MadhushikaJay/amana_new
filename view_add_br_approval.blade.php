<!DOCTYPE html>
@include('header_new')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="form_caption"> Add Break Approval</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <form class="form-horizontal" action="save_br_approval" method="post" id="form">
                {{csrf_field()}}

                    @if(count($errors))
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                        </div>
                    @endif
                    
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            <span class="glyphicon glyphicon-ok">
                            </span>
                            <em> {!! session('flash_message') !!}</em>
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left:0px;">
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">Week Days</label>
                                    </div>
                                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                    <select class="form-control select2 custom-select-value" name="week_day" id="week_day" style="width: 100%" required>
                                            <option value="All">Select Day</option>
                                            <option value="Sunday">Sunday</option>
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">Start Time</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 15px; padding-right: 0px;">
                                        <input type="time" class="some_class" 
                                        value="<?php echo date('00:00:00'); ?>" name="frm_time" id="frm_time">
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="username" class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important;">End Time</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 15px; padding-right: 0px;">
                                        <input type="time" class="some_class" 
                                        value="<?php echo date('23:00:00'); ?>" name="end_time" id="end_time">
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label for="userstatus" class="login2 pull-right pull-right-pro required">Status</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"  style="padding-top: 15px; padding-right: 0px;">
                                        <input type="radio" name="userstatus" value="1" required="" style="width: 10%;"> Active
                                        <input type="radio" name="userstatus" value="0" required="" style="width: 10%;"> Inactive
                                    </div>
                               </div>
                            </div>    
                            </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" margin-top:10px;">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2" style="margin-left: 61px;">
                                <button type="submit" class="btn btn-custon-four btn-success attr_btn" name="submit">Submit</button>
                                <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="cancel()">Cancel</button>
                        </div>
                    </div> 
                </form>
			</div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    function cancel() {
        location.href = "{{url ('view_br_approval_master')}} ";
    }
</script>
<script>


$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
@include('footer')

</body>
</html>
      
